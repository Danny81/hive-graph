# JRC-Live - Voest: Database

This project requires a Postgres database with version 11.5 or higher

## Setup
1. Either install a Postgres Database locally or create one in Amazon RDS. Standard parameters should in both cases be sufficient.

2. After setup, create a new empty database in the previously launched instance

3. Run the `database_ddl.sql` script. This will create a schema `voestbmwgraph` which holds all necessary DB objects for the prototype

4. Import data via the Angular Client GUI

5. Optionally: Populate the `path_table` for selected nodes or maybe even all nodes. This will speed up the graph expansion in the client significantly. Connect with a SQL IDE to the database and run something like 

`insert into path_table select 'bmw gr', node1, node2, depth, val, cap, sharecustomercogsfrom getnextxedges('bmw gr', 3);`

where `bmw gr` is the id of the node that you want to pre-load in the database and `3` is the number of neighbour degrees that should be pre-calculated for this node. Depending on the graph structure, this precalculation might consume lots of time.


