/*as POSTGRES user */
create schema voestbmwgraph;

create user voestbmwgraph with password '???';


alter schema voestbmwgraph owner to voestbmwgraph;

/*as voestbmwgraph user */
create table if not exists industry
(
	industry_id text not null
		constraint industry_pkey
			primary key,
	sector text not null,
	industrygroup text not null,
	industry text not null,
	subindustry text not null
);

alter table industry owner to voestbmwgraph;

create table if not exists node
(
	node_id text not null
		constraint node_pkey
			primary key,
	name text not null,
	industry_id text not null
		constraint node_industry_id_fkey
			references industry,
	country text,
	cogs numeric,
	capex numeric,
	finance_in numeric,
	finance_out numeric,
	competitor_token varchar,
	showlabel boolean default false,
	degree integer,
	numeric_id serial not null
);

alter table node owner to voestbmwgraph;

create index if not exists node_competitor_token_index
	on node (competitor_token);

create table if not exists edge
(
	from_node_id text not null
		constraint edge_from_node_id_fkey
			references node,
	to_node_id text not null
		constraint edge_to_node_id_fkey
			references node,
	value numeric,
	capex numeric,
	share_customer_cogs numeric,
	constraint edge_pkey
		primary key (from_node_id, to_node_id)
);

alter table edge owner to voestbmwgraph;

create index if not exists edge_from_node_id_index
	on edge (from_node_id);

create index if not exists edge_to_node_id_index
	on edge (to_node_id);

create unique index if not exists edge_pk_index
	on edge (from_node_id, to_node_id);

create table if not exists path_table
(
	startnode text
		constraint path_table_node_node_id_fk
			references node
				on update cascade on delete cascade,
	node1 text,
	node2 text,
	depth integer,
	val numeric,
	cap numeric,
	sharecustomercogs numeric
);

alter table path_table owner to voestbmwgraph;

create index if not exists path_table_index
	on path_table (startnode, depth);

create or replace function getnextxedges(mystartnode text, maxdepth integer) returns TABLE(node1 text, node2 text, depth integer, val numeric, cap numeric, sharecustomercogs numeric)
	language plpgsql
as $$
DECLARE
        currDepth int := 0;
        var_r record;
        searchOrigins text [];
        searchNode text;
        nextOrigins text [];
        allOrigins text [];
        cnt int;
        inPathTable bool = false;
    BEGIN

        searchOrigins  := array_append(searchOrigins, myStartnode);
        --allOrigins :=  array_append(searchOrigins, startNode);

        for var_r in(   select
                                    voe.node1,
                                    voe.node2,
                                    voe.val,
                                    voe.depth,
                                    voe.cap,
                                    voe.sharecustomercogs
                                from
                                    voestbmwgraph.path_table voe
                                where voe.startnode = myStartnode and voe.depth<=maxDepth /*or to_node_id = searchNode*/ )
        loop
            inPathTable = true;
            node1 := var_r.node1;
            node2 := var_r.node2;
            depth := var_r.depth;
            val := var_r.val;
            cap := var_r.cap;
            shareCustomerCogs := var_r.sharecustomercogs;
            return next;

        end loop;

        if (not inPathTable) then

            while currDepth < maxDepth loop

                foreach searchNode in array searchOrigins loop

                    for var_r in(   select
                                        from_node_id,
                                        to_node_id,
                                        value,
                                        capex,
                                        share_customer_cogs
                                    from
                                        voestbmwgraph.edge
                                    where from_node_id = searchNode or to_node_id = searchNode )
                    loop

                        select count(*) into cnt
                        where searchNode = any(allOrigins);
                        if (cnt=0) then
                        node1 := var_r.from_node_id;
                        node2 := var_r.to_node_id;
                        depth := currDepth+1;
                        val := var_r.value;
                        cap := var_r.capex;
                        shareCustomerCogs := var_r.share_customer_cogs;

                        nextOrigins  := array_append(nextOrigins, var_r.to_node_id);
                        nextOrigins  := array_append(nextOrigins, var_r.from_node_id);

                        return next;
                        end if;
                    end loop;

                    select count(*) into cnt
                        where searchNode = any(allOrigins);

                    if (cnt=0) then
                        allOrigins  := array_append(allOrigins, searchNode);
                    end if;

                end loop;

                searchOrigins := nextOrigins;
                nextOrigins := null ;
                currDepth := currDepth+1;

            end loop;
        end if;
    END;
$$;

alter function getnextxedges(text, integer) owner to voestbmwgraph;

