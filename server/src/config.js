export const DB_JS = 'JS';
export const DB_POSTGRES = 'Postgres';
export const DB_ELASTIC_SEARCH = 'ElasticSearch';

export const CONFIG = {
  db: DB_POSTGRES,
  port: 8080,
  schemaName: 'voestbmwgraph',
  corsOrigins: {
    dev: '*',
    prod: '*',
  },
  vis: {
    topInfluencer: 3, // Top X Influencer nodes according to eigenvector centrality in graph metrics will be marked in graph
    topGateKeeper: 3, // Top X GateKeeper nodes according to betweenness centrality in graph will be marked in graph
    topPopular: 3, //Top X Popular Nodes according to Degree  in graph will be marked in graph
    topBridges: 3, //Top X links according to edge betweenness centrality will be marked in graph
  },
};
