import bodyParser from 'body-parser';
import cors from 'cors';
import 'dotenv/config';
import express from 'express';
import { CONFIG, DB_POSTGRES } from './config';
import routes from './controller/routes';
import { errorMiddleware } from './middleware/error-middleware';
import { sequelize } from './repository/postgres/models/index';

// start app
const start = () => {
  const app = express();



  // register middleware
  app.use(cors({
    origin: CONFIG.corsOrigins.dev,
    optionsSuccessStatus: 200,
    limit: '50mb',
  }));

  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

  //app.use(bodyParser({limit: '50mb'}));

  // app routes
  app.use('/rest/industries', routes.industries);
  app.use('/rest/network', routes.network);
  app.use('/rest/metrics', routes.metrics);

  // error handling
  app.use('*', routes.notFound);
  app.use(errorMiddleware);

  app.listen(CONFIG.port, () => {
    console.log(`Listening on port ${CONFIG.port}!`);
  });
};

if (CONFIG.db === DB_POSTGRES) {
  console.log(`Postgres connection string  ${process.env.POSTGRES_DB_CONNECTION_STRING}`);
  sequelize.sync().then(start);
} else {
  start();
}
