import { Router } from 'express';
import { asyncMiddleware } from '../middleware/async-middleware';
import { industryService } from '../service/industry.service';
import {networkService} from "../service/network.service";

const router = Router();

router.get('/tree', asyncMiddleware(async (req, res, next) => {
  const result = await industryService.getIndustryTree();
  res.json(result);
}));

router.get('/maintenance/deleteall', asyncMiddleware(async (req, res, next) => {
  const result = await industryService.deleteAll();
  res.json(result);
}));

router.post('/maintenance/import', asyncMiddleware(async (req, res, next) => {
  const result = await industryService.import(req.body);
  res.json(result);
}));

export default router;
