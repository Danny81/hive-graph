import { Router } from 'express';
import { asyncMiddleware } from '../middleware/async-middleware';
import { linkService } from '../service/link.service';
import { networkService } from '../service/network.service';
import { nodeService } from '../service/node.service';
import {industryService} from "../service/industry.service";

const router = Router();

function getBody(req) {
  return req.body && Object.keys(req.body).length > 0 ? req.body : undefined;
}

router.get('/', asyncMiddleware(async (req, res, next) => {
  const result = await networkService.getFullNetwork();
  res.json(result);
}));

router.post('/', asyncMiddleware(async (req, res, next) => {
  const result = await networkService.getFilteredNetwork(getBody(req));
  res.json(result);
}));

router.get('/available-filters', asyncMiddleware(async (req, res, next) => {
  const result = await networkService.getAvailableFilters();
  res.json(result);
}));

router.get('/nodes', asyncMiddleware(async (req, res, next) => {
  const result = await nodeService.findAll();
  res.json(result);
}));

router.post('/nodes/:id', asyncMiddleware(async (req, res, next) => {
  const result = await nodeService.findById(req.params.id, getBody(req));
  if (result) {
    res.json(result);
  } else {
    res.status(404).send();
  }
}));

router.post('/:id/:depth', asyncMiddleware(async (req, res, next) => {
  const result = await networkService.getNextXTiers(req.params.id, req.params.depth, getBody(req));
  if (result) {
    res.json(result);
  } else {
    res.status(404).send();
  }
}));

router.get('/nodes/competitors/:egonode/:anonymize', asyncMiddleware(async (req, res, next) => {
  const result = await nodeService.getCompetitors(req.params.egonode, req.params.anonymize);
  if (result) {
    res.json(result);
  } else {
    res.status(404).send();
  }
}));

router.get('/links', asyncMiddleware(async (req, res, next) => {
  const result = await linkService.findAll();
  res.json(result);
}));

router.get('/links/:id/:anonymize', asyncMiddleware(async (req, res, next) => {
  const result = await linkService.findById(req.params.id, req.params.anonymize);
  res.json(result);
}));

//Maintenance
router.get('/maintenance/links/deleteall', asyncMiddleware(async (req, res, next) => {
  const result = await linkService.deleteAll();
  res.json(result);
}));

router.get('/maintenance/nodes/deleteall', asyncMiddleware(async (req, res, next) => {
  const result = await nodeService.deleteAll();
  res.json(result);
}));

router.post('/maintenance/nodes/import', asyncMiddleware(async (req, res, next) => {
  const result = await nodeService.import(req.body);
  res.json(result);
}));

router.post('/maintenance/links/import', asyncMiddleware(async (req, res, next) => {
  const result = await linkService.import(req.body);
  res.json(result);
}));

export default router;
