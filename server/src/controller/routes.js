import network from './network.controller';
import industries from './industry.controller';
import metrics from './metrics.controller';

export default {
  industries,
  network,
  metrics,
  notFound: (req, res) => res.status(404).json(),
};
