import { Router } from 'express';
import { asyncMiddleware } from '../middleware/async-middleware';
import {metricsService} from "../service/metrics.service";


const router = Router();

router.get('/add/:interaction/:id/:detectiontime/:completiontime/:elapsedtime', asyncMiddleware(async (req, res, next) => {
   const result = await metricsService.addMeasurement(req.params.interaction, req.params.id, req.params.detectiontime, req.params.completiontime, req.params.elapsedtime);
    if (result) {
        res.json(result);
    } else {
        res.status(404).send();
    }
}));

export default router;
