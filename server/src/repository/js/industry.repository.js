import { mappingService } from '../mapping.service';
import { INDUSTRIES } from './data/industries';

function IndustryRepository() {

  this.industries = INDUSTRIES.map(mappingService.mapIndustry);

  this.findAll = () => {
    return this.industries;
  };

}

export const jsIndustryRepository = new IndustryRepository();
