export const INDUSTRIES = [
  {
    "industry_id": "25201010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Household Durables",
    "subindustry": "Consumer Electronics"
  },
  {
    "industry_id": "20106015",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Machinery",
    "subindustry": "Agricultural & Farm Machinery"
  },
  {
    "industry_id": "15103020",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Containers & Packaging",
    "subindustry": "Paper Packaging"
  },
  {
    "industry_id": "50101010",
    "sector": "Communication Services",
    "industrygroup": "Telecommunication Services",
    "industry": "Diversified Telecommunication Services",
    "subindustry": "Alternative Carriers"
  },
  {
    "industry_id": "20302010",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Airlines",
    "subindustry": "Airlines"
  },
  {
    "industry_id": "60102020",
    "sector": "Real Estate",
    "industrygroup": "Real Estate",
    "industry": "Real Estate Management & Development",
    "subindustry": "Real Estate Operating Companies"
  },
  {
    "industry_id": "55101010",
    "sector": "Utilities",
    "industrygroup": "Utilities",
    "industry": "Electric Utilities",
    "subindustry": "Electric Utilities"
  },
  {
    "industry_id": "40101010",
    "sector": "Financials",
    "industrygroup": "Banks",
    "industry": "Banks",
    "subindustry": "Diversified Banks"
  },
  {
    "industry_id": "10101010",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Energy Equipment & Services",
    "subindustry": "Oil & Gas Drilling"
  },
  {
    "industry_id": "45201020",
    "sector": "Information Technology",
    "industrygroup": "Technology Hardware & Equipment",
    "industry": "Communications Equipment",
    "subindustry": "Communications Equipment"
  },
  {
    "industry_id": "25504060",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Specialty Retail",
    "subindustry": "Homefurnishing Retail"
  },
  {
    "industry_id": "50203010",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Interactive Media & Services",
    "subindustry": "Interactive Media & Services"
  },
  {
    "industry_id": "50101020",
    "sector": "Communication Services",
    "industrygroup": "Telecommunication Services",
    "industry": "Diversified Telecommunication Services",
    "subindustry": "Integrated Telecommunication Services"
  },
  {
    "industry_id": "40201040",
    "sector": "Financials",
    "industrygroup": "Diversified Financials",
    "industry": "Diversified Financial Services",
    "subindustry": "Specialized Finance"
  },
  {
    "industry_id": "20106020",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Machinery",
    "subindustry": "Industrial Machinery"
  },
  {
    "industry_id": "60101080",
    "sector": "Real Estate",
    "industrygroup": "Real Estate",
    "industry": "Equity Real Estate Investment Trusts (REITs)",
    "subindustry": "Specialized REITs"
  },
  {
    "industry_id": "25102020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Automobiles & Components",
    "industry": "Automobiles",
    "subindustry": "Motorcycle Manufacturers"
  },
  {
    "industry_id": "35101010",
    "sector": "Health Care",
    "industrygroup": "Health Care Equipment & Services",
    "industry": "Health Care Equipment & Supplies",
    "subindustry": "Health Care Equipment"
  },
  {
    "industry_id": "45301010",
    "sector": "Information Technology",
    "industrygroup": "Semiconductors & Semiconductor Equipment",
    "industry": "Semiconductors & Semiconductor Equipment",
    "subindustry": "Semiconductor Equipment"
  },
  {
    "industry_id": "60102030",
    "sector": "Real Estate",
    "industrygroup": "Real Estate",
    "industry": "Real Estate Management & Development",
    "subindustry": "Real Estate Development"
  },
  {
    "industry_id": "25101020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Automobiles & Components",
    "industry": "Auto Components",
    "subindustry": "Tires & Rubber"
  },
  {
    "industry_id": "30202010",
    "sector": "Consumer Staples",
    "industrygroup": "Food. Beverage & Tobacco",
    "industry": "Food Products",
    "subindustry": "Agricultural Products"
  },
  {
    "industry_id": "35102010",
    "sector": "Health Care",
    "industrygroup": "Health Care Equipment & Services",
    "industry": "Health Care Providers & Services",
    "subindustry": "Health Care Distributors"
  },
  {
    "industry_id": "30301010",
    "sector": "Consumer Staples",
    "industrygroup": "Household & Personal Products",
    "industry": "Household Products",
    "subindustry": "Household Products"
  },
  {
    "industry_id": "45103020",
    "sector": "Information Technology",
    "industrygroup": "Software & Services",
    "industry": "Software",
    "subindustry": "Systems Software"
  },
  {
    "industry_id": "45203010",
    "sector": "Information Technology",
    "industrygroup": "Technology Hardware & Equipment",
    "industry": "Electronic Equipment. Instruments & Components",
    "subindustry": "Electronic Equipment & Instruments"
  },
  {
    "industry_id": "20304010",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Road & Rail",
    "subindustry": "Railroads"
  },
  {
    "industry_id": "25301030",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Services",
    "industry": "Hotels. Restaurants & Leisure",
    "subindustry": "Leisure Facilities"
  },
  {
    "industry_id": "15104050",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Steel"
  },
  {
    "industry_id": "15105020",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Paper & Forest Products",
    "subindustry": "Paper Products"
  },
  {
    "industry_id": "50201040",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Media",
    "subindustry": "Publishing"
  },
  {
    "industry_id": "35103010",
    "sector": "Health Care",
    "industrygroup": "Health Care Equipment & Services",
    "industry": "Health Care Technology",
    "subindustry": "Health Care Technology"
  },
  {
    "industry_id": "35201010",
    "sector": "Health Care",
    "industrygroup": "Pharmaceuticals. Biotechnology & Life Sciences",
    "industry": "Biotechnology",
    "subindustry": "Biotechnology"
  },
  {
    "industry_id": "15104045",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Silver"
  },
  {
    "industry_id": "25503020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Multiline Retail",
    "subindustry": "General Merchandise Stores"
  },
  {
    "industry_id": "25302020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Services",
    "industry": "Diversified Consumer Services",
    "subindustry": "Specialized Consumer Services"
  },
  {
    "industry_id": "15104025",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Copper"
  },
  {
    "industry_id": "30203010",
    "sector": "Consumer Staples",
    "industrygroup": "Food. Beverage & Tobacco",
    "industry": "Tobacco",
    "subindustry": "Tobacco"
  },
  {
    "industry_id": "10102010",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Oil. Gas & Consumable Fuels",
    "subindustry": "Integrated Oil & Gas"
  },
  {
    "industry_id": "35202010",
    "sector": "Health Care",
    "industrygroup": "Pharmaceuticals. Biotechnology & Life Sciences",
    "industry": "Pharmaceuticals",
    "subindustry": "Pharmaceuticals"
  },
  {
    "industry_id": "50102010",
    "sector": "Communication Services",
    "industrygroup": "Telecommunication Services",
    "industry": "Wireless Telecommunication Services",
    "subindustry": "Wireless Telecommunication Services"
  },
  {
    "industry_id": "55105020",
    "sector": "Utilities",
    "industrygroup": "Utilities",
    "industry": "Independent Power and Renewable Electricity Producers",
    "subindustry": "Renewable Electricity"
  },
  {
    "industry_id": "45202030",
    "sector": "Information Technology",
    "industrygroup": "Technology Hardware & Equipment",
    "industry": "Technology Hardware. Storage & Peripherals",
    "subindustry": "Technology Hardware. Storage & Peripherals"
  },
  {
    "industry_id": "45203015",
    "sector": "Information Technology",
    "industrygroup": "Technology Hardware & Equipment",
    "industry": "Electronic Equipment. Instruments & Components",
    "subindustry": "Electronic Components"
  },
  {
    "industry_id": "10102020",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Oil. Gas & Consumable Fuels",
    "subindustry": "Oil & Gas Exploration & Production"
  },
  {
    "industry_id": "40301030",
    "sector": "Financials",
    "industrygroup": "Insurance",
    "industry": "Insurance",
    "subindustry": "Multi-line Insurance"
  },
  {
    "industry_id": "25101010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Automobiles & Components",
    "industry": "Auto Components",
    "subindustry": "Auto Parts & Equipment"
  },
  {
    "industry_id": "20202020",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Professional Services",
    "subindustry": "Research & Consulting Services"
  },
  {
    "industry_id": "25504050",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Specialty Retail",
    "subindustry": "Automotive Retail"
  },
  {
    "industry_id": "45102020",
    "sector": "Information Technology",
    "industrygroup": "Software & Services",
    "industry": "IT Services",
    "subindustry": "Data Processing & Outsourced Services"
  },
  {
    "industry_id": "60101010",
    "sector": "Real Estate",
    "industrygroup": "Real Estate",
    "industry": "Equity Real Estate Investment Trusts (REITs)",
    "subindustry": "Diversified REITs"
  },
  {
    "industry_id": "25504040",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Specialty Retail",
    "subindustry": "Specialty Stores"
  },
  {
    "industry_id": "20104020",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Electrical Equipment",
    "subindustry": "Heavy Electrical Equipment"
  },
  {
    "industry_id": "45301020",
    "sector": "Information Technology",
    "industrygroup": "Semiconductors & Semiconductor Equipment",
    "industry": "Semiconductors & Semiconductor Equipment",
    "subindustry": "Semiconductors"
  },
  {
    "industry_id": "25203030",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Textiles. Apparel & Luxury Goods",
    "subindustry": "Textiles"
  },
  {
    "industry_id": "25301020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Services",
    "industry": "Hotels. Restaurants & Leisure",
    "subindustry": "Hotels. Resorts & Cruise Lines"
  },
  {
    "industry_id": "25502020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Internet & Direct Marketing Retail",
    "subindustry": "Internet & Direct Marketing Retail"
  },
  {
    "industry_id": "25501010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Distributors",
    "subindustry": "Distributors"
  },
  {
    "industry_id": "30101030",
    "sector": "Consumer Staples",
    "industrygroup": "Food & Staples Retailing",
    "industry": "Food & Staples Retailing",
    "subindustry": "Food Retail"
  },
  {
    "industry_id": "50201010",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Media",
    "subindustry": "Advertising"
  },
  {
    "industry_id": "15104020",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Diversified Metals & Mining"
  },
  {
    "industry_id": "40203020",
    "sector": "Financials",
    "industrygroup": "Diversified Financials",
    "industry": "Capital Markets",
    "subindustry": "Investment Banking & Brokerage"
  },
  {
    "industry_id": "50202010",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Entertainment",
    "subindustry": "Movies & Entertainment"
  },
  {
    "industry_id": "10101020",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Energy Equipment & Services",
    "subindustry": "Oil & Gas Equipment & Services"
  },
  {
    "industry_id": "20201060",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Commercial Services & Supplies",
    "subindustry": "Office Services & Supplies"
  },
  {
    "industry_id": "15105010",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Paper & Forest Products",
    "subindustry": "Forest Products"
  },
  {
    "industry_id": "55103010",
    "sector": "Utilities",
    "industrygroup": "Utilities",
    "industry": "Multi-Utilities",
    "subindustry": "Multi-Utilities"
  },
  {
    "industry_id": "#NV",
    "sector": "#NV",
    "industrygroup": "#NV",
    "industry": "#NV",
    "subindustry": "#NV"
  },
  {
    "industry_id": "20304020",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Road & Rail",
    "subindustry": "Trucking"
  },
  {
    "industry_id": "30202030",
    "sector": "Consumer Staples",
    "industrygroup": "Food. Beverage & Tobacco",
    "industry": "Food Products",
    "subindustry": "Packaged Foods & Meats"
  },
  {
    "industry_id": "55104010",
    "sector": "Utilities",
    "industrygroup": "Utilities",
    "industry": "Water Utilities",
    "subindustry": "Water Utilities"
  },
  {
    "industry_id": "15101030",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Chemicals",
    "subindustry": "Fertilizers & Agricultural Chemicals"
  },
  {
    "industry_id": "40301020",
    "sector": "Financials",
    "industrygroup": "Insurance",
    "industry": "Insurance",
    "subindustry": "Life & Health Insurance"
  },
  {
    "industry_id": "25201050",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Household Durables",
    "subindustry": "Housewares & Specialties"
  },
  {
    "industry_id": "15104030",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Gold"
  },
  {
    "industry_id": "30201010",
    "sector": "Consumer Staples",
    "industrygroup": "Food. Beverage & Tobacco",
    "industry": "Beverages",
    "subindustry": "Brewers"
  },
  {
    "industry_id": "20107010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Trading Companies & Distributors",
    "subindustry": "Trading Companies & Distributors"
  },
  {
    "industry_id": "55102010",
    "sector": "Utilities",
    "industrygroup": "Utilities",
    "industry": "Gas Utilities",
    "subindustry": "Gas Utilities"
  },
  {
    "industry_id": "15103010",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Containers & Packaging",
    "subindustry": "Metal & Glass Containers"
  },
  {
    "industry_id": "20201070",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Commercial Services & Supplies",
    "subindustry": "Diversified Support Services"
  },
  {
    "industry_id": "30201020",
    "sector": "Consumer Staples",
    "industrygroup": "Food. Beverage & Tobacco",
    "industry": "Beverages",
    "subindustry": "Distillers & Vintners"
  },
  {
    "industry_id": "20305020",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Transportation Infrastructure",
    "subindustry": "Highways & Railtracks"
  },
  {
    "industry_id": "25201040",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Household Durables",
    "subindustry": "Household Appliances"
  },
  {
    "industry_id": "25102010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Automobiles & Components",
    "industry": "Automobiles",
    "subindustry": "Automobile Manufacturers"
  },
  {
    "industry_id": "20201050",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Commercial Services & Supplies",
    "subindustry": "Environmental & Facilities Services"
  },
  {
    "industry_id": "25504030",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Specialty Retail",
    "subindustry": "Home Improvement Retail"
  },
  {
    "industry_id": "20305030",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Transportation Infrastructure",
    "subindustry": "Marine Ports & Services"
  },
  {
    "industry_id": "40203010",
    "sector": "Financials",
    "industrygroup": "Diversified Financials",
    "industry": "Capital Markets",
    "subindustry": "Asset Management & Custody Banks"
  },
  {
    "industry_id": "#N/A N/A",
    "sector": "#NV",
    "industrygroup": "#NV",
    "industry": "#NV",
    "subindustry": "#NV"
  },
  {
    "industry_id": "25203010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Textiles. Apparel & Luxury Goods",
    "subindustry": "Apparel. Accessories & Luxury Goods"
  },
  {
    "industry_id": "45102010",
    "sector": "Information Technology",
    "industrygroup": "Software & Services",
    "industry": "IT Services",
    "subindustry": "IT Consulting & Other Services"
  },
  {
    "industry_id": "30201030",
    "sector": "Consumer Staples",
    "industrygroup": "Food. Beverage & Tobacco",
    "industry": "Beverages",
    "subindustry": "Soft Drinks"
  },
  {
    "industry_id": "45203020",
    "sector": "Information Technology",
    "industrygroup": "Technology Hardware & Equipment",
    "industry": "Electronic Equipment. Instruments & Components",
    "subindustry": "Electronic Manufacturing Services"
  },
  {
    "industry_id": "20303010",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Marine",
    "subindustry": "Marine"
  },
  {
    "industry_id": "45203030",
    "sector": "Information Technology",
    "industrygroup": "Technology Hardware & Equipment",
    "industry": "Electronic Equipment. Instruments & Components",
    "subindustry": "Technology Distributors"
  },
  {
    "industry_id": "10102030",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Oil. Gas & Consumable Fuels",
    "subindustry": "Oil & Gas Refining & Marketing"
  },
  {
    "industry_id": "35101020",
    "sector": "Health Care",
    "industrygroup": "Health Care Equipment & Services",
    "industry": "Health Care Equipment & Supplies",
    "subindustry": "Health Care Supplies"
  },
  {
    "industry_id": "30101010",
    "sector": "Consumer Staples",
    "industrygroup": "Food & Staples Retailing",
    "industry": "Food & Staples Retailing",
    "subindustry": "Drug Retail"
  },
  {
    "industry_id": "40301040",
    "sector": "Financials",
    "industrygroup": "Insurance",
    "industry": "Insurance",
    "subindustry": "Property & Casualty Insurance"
  },
  {
    "industry_id": "35102015",
    "sector": "Health Care",
    "industrygroup": "Health Care Equipment & Services",
    "industry": "Health Care Providers & Services",
    "subindustry": "Health Care Services"
  },
  {
    "industry_id": "30302010",
    "sector": "Consumer Staples",
    "industrygroup": "Household & Personal Products",
    "industry": "Personal Products",
    "subindustry": "Personal Products"
  },
  {
    "industry_id": "10102050",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Oil. Gas & Consumable Fuels",
    "subindustry": "Coal & Consumable Fuels"
  },
  {
    "industry_id": "45102030",
    "sector": "Information Technology",
    "industrygroup": "Software & Services",
    "industry": "IT Services",
    "subindustry": "Internet Services & Infrastructure"
  },
  {
    "industry_id": "50202020",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Entertainment",
    "subindustry": "Interactive Home Entertainment"
  },
  {
    "industry_id": "20106010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Machinery",
    "subindustry": "Construction Machinery & Heavy Trucks"
  },
  {
    "industry_id": "35203010",
    "sector": "Health Care",
    "industrygroup": "Pharmaceuticals. Biotechnology & Life Sciences",
    "industry": "Life Sciences Tools & Services",
    "subindustry": "Life Sciences Tools & Services"
  },
  {
    "industry_id": "20305010",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Transportation Infrastructure",
    "subindustry": "Airport Services"
  },
  {
    "industry_id": "40201030",
    "sector": "Financials",
    "industrygroup": "Diversified Financials",
    "industry": "Diversified Financial Services",
    "subindustry": "Multi-Sector Holdings"
  },
  {
    "industry_id": "20201080",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Commercial Services & Supplies",
    "subindustry": "Security & Alarm Services"
  },
  {
    "industry_id": "15101010",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Chemicals",
    "subindustry": "Commodity Chemicals"
  },
  {
    "industry_id": "60101070",
    "sector": "Real Estate",
    "industrygroup": "Real Estate",
    "industry": "Equity Real Estate Investment Trusts (REITs)",
    "subindustry": "Retail REITs"
  },
  {
    "industry_id": "25503010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Multiline Retail",
    "subindustry": "Department Stores"
  },
  {
    "industry_id": "25203020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Textiles. Apparel & Luxury Goods",
    "subindustry": "Footwear"
  },
  {
    "industry_id": "20103010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Construction & Engineering",
    "subindustry": "Construction & Engineering"
  },
  {
    "industry_id": "25201030",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Household Durables",
    "subindustry": "Homebuilding"
  },
  {
    "industry_id": "35102020",
    "sector": "Health Care",
    "industrygroup": "Health Care Equipment & Services",
    "industry": "Health Care Providers & Services",
    "subindustry": "Health Care Facilities"
  },
  {
    "industry_id": "10102040",
    "sector": "Energy",
    "industrygroup": "Energy",
    "industry": "Oil. Gas & Consumable Fuels",
    "subindustry": "Oil & Gas Storage & Transportation"
  },
  {
    "industry_id": "55105010",
    "sector": "Utilities",
    "industrygroup": "Utilities",
    "industry": "Independent Power and Renewable Electricity Producers",
    "subindustry": "Independent Power Producers & Energy Traders"
  },
  {
    "industry_id": "25504020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Specialty Retail",
    "subindustry": "Computer & Electronics Retail"
  },
  {
    "industry_id": "50201030",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Media",
    "subindustry": "Cable & Satellite"
  },
  {
    "industry_id": "50201020",
    "sector": "Communication Services",
    "industrygroup": "Media & Entertainment",
    "industry": "Media",
    "subindustry": "Broadcasting"
  },
  {
    "industry_id": "15104010",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Aluminum"
  },
  {
    "industry_id": "30101040",
    "sector": "Consumer Staples",
    "industrygroup": "Food & Staples Retailing",
    "industry": "Food & Staples Retailing",
    "subindustry": "Hypermarkets & Super Centers"
  },
  {
    "industry_id": "25201020",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Household Durables",
    "subindustry": "Home Furnishings"
  },
  {
    "industry_id": "25302010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Services",
    "industry": "Diversified Consumer Services",
    "subindustry": "Education Services"
  },
  {
    "industry_id": "25301010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Services",
    "industry": "Hotels. Restaurants & Leisure",
    "subindustry": "Casinos & Gaming"
  },
  {
    "industry_id": "15101040",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Chemicals",
    "subindustry": "Industrial Gases"
  },
  {
    "industry_id": "45103010",
    "sector": "Information Technology",
    "industrygroup": "Software & Services",
    "industry": "Software",
    "subindustry": "Application Software"
  },
  {
    "industry_id": "15102010",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Construction Materials",
    "subindustry": "Construction Materials"
  },
  {
    "industry_id": "20104010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Electrical Equipment",
    "subindustry": "Electrical Components & Equipment"
  },
  {
    "industry_id": "20102010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Building Products",
    "subindustry": "Building Products"
  },
  {
    "industry_id": "20105010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Industrial Conglomerates",
    "subindustry": "Industrial Conglomerates"
  },
  {
    "industry_id": "#N/A Invalid Security",
    "sector": "#NV",
    "industrygroup": "#NV",
    "industry": "#NV",
    "subindustry": "#NV"
  },
  {
    "industry_id": "25301040",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Services",
    "industry": "Hotels. Restaurants & Leisure",
    "subindustry": "Restaurants"
  },
  {
    "industry_id": "40202010",
    "sector": "Financials",
    "industrygroup": "Diversified Financials",
    "industry": "Consumer Finance",
    "subindustry": "Consumer Finance"
  },
  {
    "industry_id": "20201010",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Commercial Services & Supplies",
    "subindustry": "Commercial Printing"
  },
  {
    "industry_id": "20101010",
    "sector": "Industrials",
    "industrygroup": "Capital Goods",
    "industry": "Aerospace & Defense",
    "subindustry": "Aerospace & Defense"
  },
  {
    "industry_id": "60102010",
    "sector": "Real Estate",
    "industrygroup": "Real Estate",
    "industry": "Real Estate Management & Development",
    "subindustry": "Diversified Real Estate Activities"
  },
  {
    "industry_id": "40101015",
    "sector": "Financials",
    "industrygroup": "Banks",
    "industry": "Banks",
    "subindustry": "Regional Banks"
  },
  {
    "industry_id": "15101050",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Chemicals",
    "subindustry": "Specialty Chemicals"
  },
  {
    "industry_id": "30101020",
    "sector": "Consumer Staples",
    "industrygroup": "Food & Staples Retailing",
    "industry": "Food & Staples Retailing",
    "subindustry": "Food Distributors"
  },
  {
    "industry_id": "15104040",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Metals & Mining",
    "subindustry": "Precious Metals & Minerals"
  },
  {
    "industry_id": "15101020",
    "sector": "Materials",
    "industrygroup": "Materials",
    "industry": "Chemicals",
    "subindustry": "Diversified Chemicals"
  },
  {
    "industry_id": "25504010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Retailing",
    "industry": "Specialty Retail",
    "subindustry": "Apparel Retail"
  },
  {
    "industry_id": "20202010",
    "sector": "Industrials",
    "industrygroup": "Commercial & Professional Services",
    "industry": "Professional Services",
    "subindustry": "Human Resource & Employment Services"
  },
  {
    "industry_id": "20301010",
    "sector": "Industrials",
    "industrygroup": "Transportation",
    "industry": "Air Freight & Logistics",
    "subindustry": "Air Freight & Logistics"
  },
  {
    "industry_id": "25202010",
    "sector": "Consumer Discretionary",
    "industrygroup": "Consumer Durables & Apparel",
    "industry": "Leisure Products",
    "subindustry": "Leisure Products"
  }
]
