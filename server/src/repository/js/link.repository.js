import { mappingService } from '../mapping.service';
import { EDGES } from './data/edges';

function LinkRepository() {

  this.links = EDGES.map(mappingService.mapLink);

  this.findAll = () => {
    return this.links;
  };

  this.findById = (id) => {
    return this.links.find(l => l.id === id);
  };

}

export const jsLinkRepository = new LinkRepository();
