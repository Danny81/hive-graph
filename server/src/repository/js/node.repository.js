import { CONFIG } from '../../config';
import { mappingService } from '../mapping.service';
import { NODES } from './data/nodes';
import { jsIndustryRepository } from './industry.repository';

function NodeRepository() {

  const industries = jsIndustryRepository.findAll();
  this.nodesMap = new Map();
  for (const node of NODES) {
    const mappedNode = mappingService.mapNode(node, industries);
    this.nodesMap.set(mappedNode.id, mappedNode);
  }

  this.findById = (id) => {
    return this.nodesMap.get(id);
  };

  this.findAll = () => {
    return this.nodes;
  };

}

export const jsNodeRepository = new NodeRepository();
