import {mappingService} from '../mapping.service';
import models from './models';

function IndustryRepository() {

  this.findAll = async () => {
    const industries = await models.Industries.findAll();
    return industries.map(mappingService.mapIndustry);
  };

  this.deleteAll = async () => {
    try {
      return await models.Industries.destroy({
        where: {},
        truncate: false
      });
    }
    catch(err) {
      return err;
    }
  };

  this.importIndustries = async (industriesJson) => {
    try {
      const addedRows = await models.Industries.bulkCreate(industriesJson);

      return true;
    }
    catch(err) {
      return err;
    }
  };

}

export const postgresIndustryRepository = new IndustryRepository();
