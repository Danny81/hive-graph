import * as Sequelize from 'sequelize';
import { CONFIG } from '../../config';
import { mappingService } from '../mapping.service';
import { postgresIndustryRepository } from './industry.repository';
import models, { sequelize } from './models';

function NodeRepository() {

  const industryRepository = postgresIndustryRepository;

  this.findAll = async () => {
    // TODO Let the DB do the JOIN-ing
    const [industries, dbRows] = await Promise.all([industryRepository.findAll(), models.Nodes.findAll()]);
    const nodes = dbRows
      .map(dbRow => mappingService.mapNode(dbRow, industries))
      .sort((a, b) => b.eigenCentrality - a.eigenCentrality);

    return nodes;
  };

  this.findById = async (id, anonymize) => {
    const [industries, dbRow] = await Promise.all([
      industryRepository.findAll(),
      models.Nodes.findByPk(id),
    ]);

    return mappingService.mapNode(dbRow, industries, anonymize);
  };

  this.findByIds = async (ids, anonymize) => {
    const sql = `select n.* from ${CONFIG.schemaName}.node n where n.node_id in (:ids);`;
    const [industries, dbRows] = await Promise.all([
      industryRepository.findAll(),
      sequelize.query(sql, {
        replacements: { ids: Array.from(ids.values()) },
        type: Sequelize.QueryTypes.SELECT,
      }),
    ]);

    return dbRows.map(dbRow => mappingService.mapNode(dbRow, industries, anonymize));
  };

  this.findXNextNeighbours = async (fromId, depth, anonymize) => {
    const [industries] = await Promise.all([industryRepository.findAll()]);
    const sql = `select n.node_id, n.name, n.industry_id, n.country, n.cogs, n.capex,
       n.finance_in, n.finance_out, n.competitor_token,
       case when n.node_id= :fromId then true else n.showlabel end,
       n.degree, n.numeric_id
        from ${CONFIG.schemaName}.node n, (
      select distinct(node2) as node from ${CONFIG.schemaName}.getNextXEdges(:fromId, :depth)
      union
      select distinct(node1) from ${CONFIG.schemaName}.getNextXEdges(:fromId, :depth)) e
      where e.node=n.node_id;`;

    const dbRows = await sequelize.query(sql, {
      replacements: { fromId: fromId, depth: depth },
      type: Sequelize.QueryTypes.SELECT,
    });

    return dbRows.map(dbRow => mappingService.mapNode(dbRow, industries, anonymize));
  };

  this.findCompetitors = async (egoId, anonymize) => {
    const [industries] = await Promise.all([industryRepository.findAll()]);
    const sql = `select n.* from ${CONFIG.schemaName}.node n 
      where n.node_id != :egoId and n.competitor_token=(
        select n2.competitor_token from ${CONFIG.schemaName}.node n2 where n2.node_id= :egoId
      ) and n.competitor_token is not null order by n.name;`;

    const dbRows = await sequelize.query(sql, {
      replacements: { egoId },
      type: Sequelize.QueryTypes.SELECT,
    });

    const convertedBool = (anonymize === 'true');

    return dbRows.map(dbRow => mappingService.mapNode(dbRow, industries, convertedBool));
  };

  this.deleteAll = async () => {
    try {
      return await models.Nodes.destroy({
        where: {},
        truncate: false
      });
    }
    catch(err) {
      return err;
    }
  };

  this.importNodes = async (nodesJson) => {
    try {
      const addedRows = await models.Nodes.bulkCreate(nodesJson);
      return true;
    }
    catch(err) {
      return err;
    }
  };

}

export const postgresNodeRepository = new NodeRepository();
