import {mappingService} from '../mapping.service';
import models from './models';

function LatencyMetricRepository() {

    this.findAll = async () => {
        const metrics = await models.LatencyMetrics.findAll();
        return metrics.map(mappingService.mapLatencyMetric);
    };

    this.deleteAll = async () => {
        try {
            return await models.LatencyMetrics.destroy({
                where: {},
                truncate: false
            });
        }
        catch(err) {
            return err;
        }
    };

    this.addMeasurement = async (interaction, nodeID, detectionTime, completionTime, elapsedTime) => {
        try {
            const addedRows = await models.LatencyMetrics.create({interaction: interaction, node_id: nodeID, detection_time: detectionTime, completion_time: completionTime,elapsed_time: elapsedTime});
            return true;
        }
        catch(err) {
            return err;
        }
    };

}

export const postgresLatencyMetricRepository = new LatencyMetricRepository();
