import {QueryTypes} from 'sequelize';
import {CONFIG} from '../../config';
import {mappingService} from '../mapping.service';
import models, {sequelize} from './models';

function LinkRepository() {

  this.findAll = async () => {
    const dbRows = await models.Edges.findAll();
    return dbRows.map(mappingService.mapLink);
  };

  this.findByNode = async (nodeId) => {
    const sql = `select e.* from ${CONFIG.schemaName}.edge e where e.from_node_id=:nodeId or e.to_node_id=:nodeId;`;
    const dbRows = await sequelize.query(sql, {
      replacements: { nodeId },
      type: QueryTypes.SELECT,
    });

    return dbRows.map(mappingService.mapLink);
  };

  this.findXNextEdges = async (fromId, depth) => {
    const sql = `select n.* from ${CONFIG.schemaName}.edge n, (
      select *  from ${CONFIG.schemaName}.getNextXEdges(:fromId, :depth) ) e
      where e.node1=n.from_node_id and e.node2=n.to_node_id;`;

    const dbRows = await sequelize.query(sql, {
      replacements: { fromId: fromId, depth: depth },
      type: QueryTypes.SELECT,
    });

    return dbRows.map(mappingService.mapLink);
  };

  this.findById = async (id) => {
    const params = mappingService.parseLinkId(id);
    const sql = `select e.* from ${CONFIG.schemaName}.edge e where e.from_node_id=:sourceId and e.to_node_id=:targetId limit 1;`;
    const dbRows = await sequelize.query(sql, {
      replacements: { sourceId: params.sourceId, targetId: params.targetId },
      type: QueryTypes.SELECT,
    });

    return dbRows.length > 0 ? mappingService.mapLink(dbRows[0]) : undefined;
  };

  this.deleteAll = async () => {
    try {
      return await models.Edges.destroy({
        where: {},
        truncate: false
      });
    }
    catch(err) {
      return err;
    }
  };

  this.importLinks = async (linksJson) => {
    try {
      const addedRows = await models.Edges.bulkCreate(linksJson);

      //update global degree
      const sql = `update  ${CONFIG.schemaName}.node n set degree=(select count(*) from edge e where e.from_node_id=n.node_id or e.to_node_id=n.node_id)`;
      const dbRows = await sequelize.query(sql, {
        type: QueryTypes.UPDATE,
      });

      return true;
    }
    catch(err) {
      return err;
    }
  };

}

export const postgresLinkRepository = new LinkRepository();
