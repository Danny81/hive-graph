import { DataTypes } from 'sequelize';
import {CONFIG} from "../../../config";

const edge = (sequelize) => {
  const Edge = sequelize.define('edge',
    {
      from_node_id: {
        type: DataTypes.STRING,
        unique: 'compositeIndex',
        primaryKey: true,
        references: {
          model: 'node',
          key: 'node_id',
        },
      },
      to_node_id: {
        type: DataTypes.STRING,
        unique: 'compositeIndex',
        primaryKey: true,
        references: {
          model: 'node',
          key: 'node_id',
        },
      },
      value: {
        type: DataTypes.DOUBLE,
      },
      capex: {
        type: DataTypes.DOUBLE,
      },
      share_customer_cogs: {
        type: DataTypes.DOUBLE,
      },
    },
    {
      tableName: 'edge',
      timestamps: false,
      schema: CONFIG.schemaName,
    });

  Edge.associate = model => {
    Edge.belongsTo(model.Nodes, { foreignKey: 'from_node_id', targetKey: 'node_id' });
  };

  Edge.associate = model => {
    Edge.belongsTo(model.Nodes, { foreignKey: 'to_node_id', targetKey: 'node_id' });
  };

    Edge.addHook('beforeBulkCreate', (edges, options) => {
        edges.forEach(function(edges, i) {if (edges.value === '' ) edges[i].value = null; });
        edges.forEach(function(edges, i) {if (edges.capex === '' ) edges[i].capex = null; });
        edges.forEach(function(edges, i) {if (edges.share_customer_cogs === '' ) edges[i].share_customer_cogs = null; });
    });

  return Edge;
};

export default edge;
