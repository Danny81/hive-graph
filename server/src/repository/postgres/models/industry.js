import {CONFIG} from "../../../config";

const industry = (sequelize, DataTypes) => {
    return sequelize.define('industry', {
        industry_id: {
            type: DataTypes.STRING,
            unique: true,
            primaryKey: true,
        },
        sector: {
            type: DataTypes.STRING,
        },
        industrygroup: {
            type: DataTypes.STRING,
        },
        industry: {
            type: DataTypes.STRING,
        },
        subindustry: {
            type: DataTypes.STRING,
        },
    },
      {
          tableName: 'industry',
          timestamps: false,
          schema: CONFIG.schemaName,
      },
    );
};

export default industry;
