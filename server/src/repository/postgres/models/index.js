import 'dotenv/config';
import { Sequelize } from 'sequelize';

const sequelize = new Sequelize(
  process.env.POSTGRES_DB_DATABASE,
  process.env.POSTGRES_DB_USER,
  process.env.POSTGRES_DB_PW,
  {
    dialect: process.env.POSTGRES_DB_DIALECT,
    host: process.env.POSTGRES_DB_HOST,
    pool: {
      max: 3,
      min: 0,
      acquire: 120000,
      idle: 120000,
    },
  },
);

const models = {
  Nodes: sequelize.import('./node'),
  Edges: sequelize.import('./edge'),
  Industries: sequelize.import('./industry'),
  LatencyMetrics: sequelize.import('./latencymetrics'),
};

Object.keys(models).forEach(key => {
  if ('associate' in models[key]) {
    models[key].associate(models);
  }
});

export { sequelize };
export default models;
