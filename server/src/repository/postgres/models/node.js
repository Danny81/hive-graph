import { DataTypes } from 'sequelize';
import {CONFIG} from "../../../config";

const node = (sequelize) => {
  const Node = sequelize.define('node', {
      node_id: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
      },
      industry_id: {
        type: DataTypes.STRING,
        references: {
          model: 'industry',
          key: 'industry_id',
        },
      },
      country: {
        type: DataTypes.STRING,
      },
      cogs: {
        type: DataTypes.DOUBLE,
      },
      capex: {
        type: DataTypes.DOUBLE,
      },
      finance_in: {
        type: DataTypes.DOUBLE,
      },
      finance_out: {
        type: DataTypes.DOUBLE,
      },
      competitor_token: {
          type: DataTypes.STRING,
      },
      showlabel: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false,
      },
      degree: {
              type: DataTypes.INTEGER,
       },
      numeric_id: {
          type: DataTypes.INTEGER,
      }
    },
    {
      tableName: 'node',
      timestamps: false,
        schema: CONFIG.schemaName,
    },
  );

  Node.associate = model => {
    Node.belongsTo(model.Industries, { foreignKey: 'industry_id', targetKey: 'industry_id' });
  };

  Node.addHook('beforeBulkCreate', (nodes, options) => {
      nodes.forEach(function(item, i) {if (item.showlabel === '' ) nodes[i].showlabel = false; });
      nodes.forEach(function(item, i) {if (item.degree === '' ) nodes[i].degree = 0; });
      nodes.forEach(function(item, i) {if (item.competitor_token === '' ) nodes[i].competitor_token = null; });
      nodes.forEach(function(item, i) {if (item.cogs === '' ) nodes[i].cogs = null; });
      nodes.forEach(function(item, i) {if (item.capex === '' ) nodes[i].capex = null; });
      nodes.forEach(function(item, i) {if (item.finance_in === '' ) nodes[i].finance_in = null; });
      nodes.forEach(function(item, i) {if (item.finance_out === '' ) nodes[i].finance_out = null; });
    });

  return Node;
};

export default node;
