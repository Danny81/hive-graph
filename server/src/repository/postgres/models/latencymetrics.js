import { DataTypes } from 'sequelize';
import {CONFIG} from "../../../config";

const latencyMetric = (sequelize) => {
    const LatencyMetric = sequelize.define('latency_metrics_table', {
            interaction: {
                type: DataTypes.STRING,
            },
            node_id: {
                type: DataTypes.STRING,
            },
            detection_time: {
                type: DataTypes.DOUBLE,
            },
            completion_time: {
                type: DataTypes.DOUBLE,
            },
            elapsed_time: {
                type: DataTypes.DOUBLE,
            }
        },
        {
            tableName: 'latency_metrics_table',
            timestamps: false,
            schema: CONFIG.schemaName,
        },
    );
    return LatencyMetric;
};

export default latencyMetric;
