import { CONFIG, DB_ELASTIC_SEARCH, DB_POSTGRES } from '../config';
import { jsIndustryRepository } from '../repository/js/industry.repository';
import { postgresIndustryRepository } from '../repository/postgres/industry.repository';
import { jsLinkRepository } from './js/link.repository';
import { jsNodeRepository } from './js/node.repository';
import { postgresLinkRepository } from './postgres/link.repository';
import { postgresNodeRepository } from './postgres/node.repository';
import {postgresLatencyMetricRepository} from "./postgres/latencyMetric.repository";

function RepositoryFactory() {

  let industryRepository;
  let nodeRepository;
  let linkRepository;
  let latencyMetricRepository;

  console.log(`Using ${CONFIG.db} as data source`);
  if (CONFIG.db === DB_POSTGRES) {
    industryRepository = postgresIndustryRepository;
    nodeRepository = postgresNodeRepository;
    linkRepository = postgresLinkRepository;
    latencyMetricRepository = postgresLatencyMetricRepository;
  } else if (CONFIG.db === DB_ELASTIC_SEARCH) {
    throw new Error('Could not create Elastic Search repositories');
  } else {
    industryRepository = jsIndustryRepository;
    nodeRepository = jsNodeRepository;
    linkRepository = jsLinkRepository;
  }

  this.getIndustryRepository = () => {
    return industryRepository;
  };

  this.getNodeRepository = () => {
    return nodeRepository;
  };

  this.getLinkRepository = () => {
    return linkRepository;
  };

  this.getLatencyMetricRepository = () => {
    return latencyMetricRepository;
  };

}

export const repositoryFactory = new RepositoryFactory();
