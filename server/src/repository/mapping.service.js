import { colorService } from '../service/color.service';

function MappingService() {

  this.mapIndustry = (dbRow) => {
    if (!dbRow) {
      return undefined;
    }

    return {
      sector: dbRow.sector,
      industryGroup: dbRow.industrygroup,
      industry: dbRow.industry,
      subIndustry: dbRow.subindustry,
      gicsSubIndustryCode: dbRow.industry_id,
    };
  };

  this.mapNode = (dbRow, industries, anonymize) => {
    if (!dbRow) {
      return undefined;
    }

    const gicsSubIndustryCode = dbRow.industry_id;
    const result = {
      id: dbRow.node_id,
      name: dbRow.name,
      country: dbRow.country,
      gicsSubIndustryCode,
      cogs: +dbRow.cogs,
      capex: +dbRow.capex,
      financeIn: +dbRow.finance_in,
      financeOut: +dbRow.finance_out,
      competitorToken: +dbRow.competitor_token,
      showlabel: +dbRow.showlabel,
      degree: +dbRow.degree,
      numeric_id: +dbRow.numeric_id,
    };

    if (anonymize) {
      result.name = 'Company ' + result.numeric_id;
    }

    const industry = industries.find(i => i.gicsSubIndustryCode === gicsSubIndustryCode);
    if (industry) {
      result.sector = industry.sector;
      result.industryGroup = industry.industryGroup;
      result.industry = industry.industry;
      result.subIndustry = industry.subIndustry;
      result.color = colorService.getSectorColor(industry.sector);
    } else {
      console.warn(`Could not find industry for ${dbRow.node_id} with gicsSubIndustryCode=${gicsSubIndustryCode}`);
    }

    return result;
  };

  this.linkId = (dbRow) => {
    return `${dbRow.from_node_id}_${dbRow.to_node_id}`;
  };

  this.parseLinkId = (id) => {
    const [sourceId, targetId] = id.split('_');
    return { sourceId, targetId };
  };

  this.mapLink = (dbRow) => {
    if (!dbRow) {
      return undefined;
    }

    return {
      id: this.linkId(dbRow),
      source: dbRow.from_node_id,
      target: dbRow.to_node_id,
      value: +dbRow.value,
      capex: +dbRow.capex,
      shareCustomerCogs: +dbRow.share_customer_cogs,
    };

  };

  this.mapLatencyMetric = (dbRow) => {
    if (!dbRow) {
      return undefined;
    }

    return {
      interaction: dbRow.interaction,
      nodeID: dbRow.node_id,
      detectionTime: dbRow.detection_time,
      completionTime: dbRow.completion_time,
      elapsedTime: dbRow.elapsed_time,
    };
  };

}

export const mappingService = new MappingService();
