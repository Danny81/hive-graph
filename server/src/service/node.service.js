import { repositoryFactory } from '../repository/repository-factory';
import { filterService } from './filter.service';

function NodeService() {

  const nodeRepository = repositoryFactory.getNodeRepository();
  const linkRepository = repositoryFactory.getLinkRepository();

  this.findAll = () => {
    return nodeRepository.findAll();
  };

  this.findById = async (id, filter) => {

    const egoNode = await nodeRepository.findById(id, filter.anonymize);
    if (egoNode) {
      const links = await linkRepository.findByNode(id);

      /*let preFilteredLinks = filter
          ? links.filter(l => filterService.filterThreshold(l.degree , filter.nodesThreshold))
          : links;*/

      let filteredLinks = filter
        ? links.filter(l => filterService.filterThreshold(l.shareCustomerCogs, filter.linksThreshold))
        : links;
      let cleanedLinks = [];

      const nodeIds = new Set();
      for (const link of filteredLinks) {
        if (link.source !== egoNode.id) {
          nodeIds.add(link.source);
        }

        if (link.target !== egoNode.id) {
          nodeIds.add(link.target);
        }
      }

      const nodes = nodeIds.size > 0 ? await nodeRepository.findByIds(nodeIds, filter.anonymize) : [];
      const nodeMap = new Map();
      nodeMap.set(egoNode.id, true);
      if (filter) {
        const selectedGicsSubIndustryCodes = new Set(filter.selectedSubIndustries ? filter.selectedSubIndustries : []);
        for (const node of nodes) {
          if (filterService.filterThreshold(node.degree, filter.nodesThreshold)
            && selectedGicsSubIndustryCodes.has(node.gicsSubIndustryCode)) {
            nodeMap.set(node.id, { node, addedSource: false, addedTarget: false });
          } else {
            nodeMap.set(node.id, false);
          }
        }
      } else {
        for (const node of nodes) {
          nodeMap.set(node.id, { node, addedSource: false, addedTarget: false });
        }
      }

      let filteredNodes = [Object.assign({}, egoNode)];
      for (const link of filteredLinks) {
        const sourceNode = nodeMap.get(link.source);
        const targetNode = nodeMap.get(link.target);
        if (!filter || (!!sourceNode && !!targetNode)) {
          if (link.source === egoNode.id) {
            const targetId = `target-${link.target}`;
            cleanedLinks.push({ ...link, target: targetId });
            if (!targetNode.addedTarget) {
              filteredNodes.push({ ...targetNode.node, originalId: link.target, id: targetId });
              targetNode.addedTarget = true;
            }
          } else if (link.target === egoNode.id) {
            const sourceId = `source-${link.source}`;
            cleanedLinks.push({ ...link, originalId: link.source, source: sourceId });
            if (sourceNode && !sourceNode.addedSource) {
              filteredNodes.push({ ...sourceNode.node, originalId: link.source, id: sourceId });
              sourceNode.addedSource = true;
            }
          }
        }
      }

      egoNode.sankey = {
        nodes: filteredNodes,
        links: cleanedLinks,
      };
    }

    return egoNode;
  };

  this.getNextXNodes = (fromId, depth) => {
    return nodeRepository.findXNextNeighbours(fromId, depth);
  };

  this.getCompetitors = (egoId, anonymize) => {
    return nodeRepository.findCompetitors(egoId, anonymize);
  };

  this.deleteAll = () => {
    return nodeRepository.deleteAll();
  };

  this.import = async (nodesJson) => {
    return nodeRepository.importNodes(nodesJson);
  };

}

export const nodeService = new NodeService();
