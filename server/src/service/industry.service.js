import { repositoryFactory } from '../repository/repository-factory';
import { colorService } from './color.service';

function IndustryService() {

  const industryRepository = repositoryFactory.getIndustryRepository();

  this.getIndustryTree = async () => {
    const industries = await industryRepository.findAll();
    const industryTree = {};
    for (const industry of industries) {
      let sectorMap = industryTree[industry.sector];
      if (!sectorMap) {
        sectorMap = { '_color': colorService.getSectorColor(industry.sector) };
        industryTree[industry.sector] = sectorMap;
      }

      let industryGroupMap = sectorMap[industry.industryGroup];
      if (!industryGroupMap) {
        industryGroupMap = {};
        sectorMap[industry.industryGroup] = industryGroupMap;
      }

      let industryMap = industryGroupMap[industry.industry];
      if (!industryMap) {
        industryMap = {};
        industryGroupMap[industry.industry] = industryMap;
      }

      industryMap[industry.subIndustry] = industry.gicsSubIndustryCode;
    }

    return industryTree;
  };

  this.deleteAll = () => {
      return industryRepository.deleteAll();
  };

  this.import = async (industriesJson) => {
    return industryRepository.importIndustries(industriesJson);
  }

}

export const industryService = new IndustryService();
