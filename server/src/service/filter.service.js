import * as jsnx from "jsnetworkx";
import { CONFIG } from '../config';

function FilterService() {

  this.filterThreshold = (n, threshold) => {
    if (threshold) {
      const hasMin = typeof threshold.min === 'number';
      const hasMax = typeof threshold.max === 'number';
      if (hasMin && hasMax) {
        return n >= threshold.min && n <= threshold.max;
      } else if (hasMin && !hasMax) {
        return n >= threshold.min;
      } else if (!hasMin && hasMax) {
        return n <= threshold.max;
      }
    }

    return true;
  };

  this.filterNetwork = (fullNetwork, filter) => {
    const filteredNetwork = { nodes: [], links: [] };

    const nodeMap = new Map();
    const selectedGicsSubIndustryCodes = new Set(filter && filter.selectedSubIndustries
      ? filter.selectedSubIndustries
      : []);

    // Rectify nodes (by degree) and remove unused links
    for (const node of fullNetwork.nodes) {
     if (!filter.showLabels) {
        node.showlabel=false;
      }

     if (filter.anonymize) {
       node.name= 'Company ' +  node.numeric_id;
     }

      if (!filter ||
        (this.filterThreshold(node.degree, filter.nodesThreshold)
          && selectedGicsSubIndustryCodes.has(node.gicsSubIndustryCode))) {
        if (!nodeMap.has(node.id)) {
          filteredNetwork.nodes.push(node);
        }
        nodeMap.set(node.id, true);
      } else {
        nodeMap.set(node.id, false);
      }
    }

    // Rectify links
    for (const link of fullNetwork.links) {
      if (nodeMap.get(link.source) && nodeMap.get(link.target)) {
        if (!filter || this.filterThreshold(link.shareCustomerCogs, filter.linksThreshold)) {
          filteredNetwork.links.push(link);
        }
      }
    }

    let cleanedNetwork = this.cleanupNetwork(filteredNetwork, filter);
    cleanedNetwork = this.calculateMetrics(cleanedNetwork, filter);
    return cleanedNetwork;

  };

  this.calculateMetrics = (network, filter) => {

    if (network.nodes.length===0) {
      //No nodes - no metrics
      return network;
    }

    var G = new jsnx.Graph();
    G.addNodesFrom(network.nodes);

    for (let k = 0; k<network.links.length; k++) {
      G.addEdge(network.links[k].source, network.links[k].target);
    }

    //degree is always calculated, the only really mandatoriy metric for the graph
    var degrees = G.degree();
    var keysDegree = Object.keys(degrees._stringValues);

    keysDegree.forEach(function(keysDegree) { //loop through keys array
      let index = network.nodes.findIndex( name => name.id ==keysDegree);
      network.nodes[index].degree = degrees._stringValues[keysDegree];
    });

    network.nodes.sort((a, b) => b.degree - a.degree);
    for(let i = 0; i< CONFIG.vis.topPopular; i++) {
      network.nodes[i].showlabel = filter.showLabels;
      network.nodes[i].topPopular = true;
    }

    //calculate other metrics if desired (default)
    if (filter.calculateMetrics) {

      var betweenness = jsnx.betweennessCentrality(G);
      var edgeBetweenness = jsnx.edgeBetweennessCentrality(G);

      var keysBetwenness = Object.keys(betweenness._stringValues); //get keys from object as an array
      var keysEdgeBetwenness = Object.keys(edgeBetweenness._keys);

      try {
        var eigenCentrality =jsnx.eigenvectorCentrality(G, 100, 100);
        var keysEigenCentrality = Object.keys(eigenCentrality._stringValues);

        keysEigenCentrality.forEach(function(keysEigenCentrality) { //loop through keys array
          let index = network.nodes.findIndex( name => name.id ==keysEigenCentrality);
          network.nodes[index].eigenCentrality = eigenCentrality._stringValues[keysEigenCentrality];
        });

      }
      catch
      {
        console.log('eigenvector centrality calculation failed - are your looking at a star graph?')
      }

      keysBetwenness.forEach(function(keysBetwenness) { //loop through keys array
        let index = network.nodes.findIndex( name => name.id ==keysBetwenness);
        network.nodes[index].betweennessCentrality = betweenness._stringValues[keysBetwenness];
      });

      keysEdgeBetwenness.forEach(function(keysEdgeBetwenness) { //loop through keys array
        let index = network.links.findIndex( name => (name.source + "," + name.target == keysEdgeBetwenness) || (name.target + "," + name.source == keysEdgeBetwenness));
        network.links[index].edgeBetweenness = edgeBetweenness._values[keysEdgeBetwenness];
      });

      network.nodes.sort((a, b) => b.betweeness_centrality - a.betweeness_centrality );
      for(let i = 0; i< CONFIG.vis.topGateKeeper; i++) {
        network.nodes[i].showlabel = filter.showLabels;
        network.nodes[i].topGateKeeper = true;
      }

      network.nodes.sort((a, b) => b.eigenCentrality  - a.eigenCentrality);
      for(let i = 0; i< CONFIG.vis.topInfluencer ; i++) {
        network.nodes[i].showlabel = filter.showLabels;
        network.nodes[i].topInfluencer = true;
      }

      network.links.sort((a, b) => b.edgeBetweenness  - a.edgeBetweenness);
      for(let i = 0; i< CONFIG.vis.topBridges ; i++) {
        network.links[i].showlabel = filter.showLabels;
        network.links[i].topBridge = true;
      }

    }

    return network;

  };

  this.cleanupNetwork = (network, filter) => {
    const cleanedNetwork = { nodes: [], links: [] };

    // Ensure all remaining nodes still have at least one link
    cleanedNetwork.nodes = network.nodes.filter(n => (!filter || n.id === filter.egoId) ||
      network.links.some(l => l.source === n.id || l.target === n.id));

    // Ensure all remaining links still have at least one node
    cleanedNetwork.links = network.links.filter(l =>
      cleanedNetwork.nodes.some(n => n.id === l.source || n.id === l.target));

    return cleanedNetwork;
  };

}

export const filterService = new FilterService();
