import { filterService } from './filter.service';
import { industryService } from './industry.service';
import { linkService } from './link.service';
import { nodeService } from './node.service';

function NetworkService() {

  this.availableFilters = undefined;

  this.getAvailableFilters = async () => {
    if (this.availableFilters) {
      return this.availableFilters;
    }

    const [industryTree, nodes, links] = await Promise.all([
      industryService.getIndustryTree(),
      nodeService.findAll(),
      linkService.findAll(),
    ]);

    let nodeMin = Number.POSITIVE_INFINITY;
    let nodeMax = 0;
    for (const node of nodes) {
      if (node.degree < nodeMin) {
        nodeMin = node.degree;
      }

      if (node.degree > nodeMax) {
        nodeMax = node.degree;
      }
    }

    let linkMin = 100;
    let linkMax = 0;
    for (const link of links) {
      if (link.shareCustomerCogs < linkMin) {
        linkMin = link.shareCustomerCogs;
      }

      if (link.shareCustomerCogs > linkMax) {
        linkMax = link.shareCustomerCogs;
      }
    }

    return this.availableFilters = {
      industryTree,
      nodes: { min: nodeMin, max: nodeMax },
      links: { min: linkMin, max: linkMax },
    };
  };

  this.getFullNetwork = async () => {
    const [nodes, links] = await Promise.all([nodeService.findAll(), linkService.findAll()]);
    return filterService.filterNetwork({ nodes, links });
  };

  this.getFilteredNetwork = async (filter) => {
    if (!filter) {
      return { nodes: [], links: [] };
    }

    const [nodes, links] = await Promise.all([nodeService.findAll(), linkService.findAll()]);
    return filterService.filterNetwork({ nodes, links }, filter);
  };

  this.getNextXTiers = async (fromId, depth, filter) => {
    const [nodes, links] = await Promise.all([
      nodeService.getNextXNodes(filter.originNodes[0], filter.originDepths[0]),
      linkService.getNextXLinks(filter.originNodes[0], filter.originDepths[0])
    ]);

    for (let i = filter.originDepths.length-1; i > 0; i--) {
      const thisNetwork = await Promise.all([
        nodeService.getNextXNodes(filter.originNodes[i], filter.originDepths[i]),
        linkService.getNextXLinks(filter.originNodes[i], filter.originDepths[i])
      ]);

      links.push(...thisNetwork[1]);
      nodes.push(...thisNetwork[0]);
    }

    return filterService.filterNetwork({ nodes, links }, filter);
  };

}

export const networkService = new NetworkService();
