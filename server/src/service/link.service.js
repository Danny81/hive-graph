import { repositoryFactory } from '../repository/repository-factory';

function LinkService() {

  const linkRepository = repositoryFactory.getLinkRepository();
  const nodeRepository = repositoryFactory.getNodeRepository();

  this.findAll = () => {
    return linkRepository.findAll();
  };

  this.findById = async (id, anonymize) => {
    const linkDetails = await linkRepository.findById(id);
    if (linkDetails) {
      const [sourceNodeDetails, targetNodeDetails] = await Promise.all([
        nodeRepository.findById(linkDetails.source, anonymize),
        nodeRepository.findById(linkDetails.target, anonymize),
      ]);

      if (sourceNodeDetails) {
        linkDetails.sourceDetails = sourceNodeDetails;
      }

      if (targetNodeDetails) {
        linkDetails.targetDetails = targetNodeDetails;
      }
    }

    return linkDetails;
  };

  this.getNextXLinks = (fromId, depth) => {
    return linkRepository.findXNextEdges(fromId, depth);
  };

  this.deleteAll = () => {
    return linkRepository.deleteAll();
  };

  this.import =  (linksJson) => {
    return linkRepository.importLinks(linksJson);
  };

}

export const linkService = new LinkService();
