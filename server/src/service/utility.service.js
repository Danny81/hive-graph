function UtilityService() {

  /**
   * https://github.com/normalized-db/core/blob/master/src/utility/deep-clone.ts
   */
  this.deepClone = (obj) => {
    const ctor = obj ? obj.constructor : null;
    if (obj === undefined || obj === null || typeof obj !== 'object' || ctor === ArrayBuffer) {
      return obj;
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects
    if (ctor === Date || ctor === RegExp || ctor === Function || ctor === JSON ||
      ctor === String || ctor === Number || ctor === Boolean ||
      ctor === Set || ctor === Map || ctor === WeakMap || ctor === WeakSet) {
      return new ctor(obj);
    }

    const result = new ctor();
    Object.keys(obj).forEach(key => result[key] = this.deepClone(obj[key]));

    return result;
  };

}

export const utilityService = new UtilityService();
