import { repositoryFactory } from '../repository/repository-factory';
import { colorService } from './color.service';

function MetricsService() {

    const latencyMetricsRepository = repositoryFactory.getLatencyMetricRepository();

    this.addMeasurement = async (interaction, nodeID, detectionTime, completionTime, elapsedTime) => {
        return latencyMetricsRepository.addMeasurement(interaction, nodeID, detectionTime, completionTime, elapsedTime);
    }

}

export const metricsService = new MetricsService();
