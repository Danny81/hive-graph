function ColorService() {

  const defaultColor = '#7F7F7F';

  const sectorColors = new Map();
  sectorColors.set('#NV', '#D62728');
  sectorColors.set('Communication Services', '#FF7F0D');
  sectorColors.set('Consumer Discretionary', '#2BA02C');
  sectorColors.set('Energy', '#18BECF');
  sectorColors.set('Financials', '#BCBD21');
  sectorColors.set('Health Care', '#E377C2');
  sectorColors.set('Industrials', '#CE9C7b');
  sectorColors.set('Information Technology', '#36EDB6');
  sectorColors.set('Materials', '#1F77B4');
  sectorColors.set('Real Estate', '#9467BD');
  sectorColors.set('Utilities', '#8C564B');

  this.getSectorColor = (sector) => {
    return sectorColors.get(sector) || defaultColor;
  };

}

export const colorService = new ColorService();
