# JRC-Live - Voest: Backend

This project was generated with NodeJS version 12.14.1

Dependencies:

<ul>
<li>Babel ( >= 7.4.4)</li>
<li>Body-Parser ( >=  1.19.0)</li>
<li>Cors ( >= 2.8.5)</li>
<li>Dotenv( >= 8.1.0)</li>
<li>Express( >= 4.17.1)</li>
<li>JSnetworkX( >= 0.3.4) - Graph Metrics</li>
<li>pg( >= 7.12.1) - Postgres DB Client</li>
<li>Sequelize( >= 5.18.4) - ORM mapping Postgres DB</li>
</ul>

## Development server
Run `nodemon --exec babel-node src/server.dev.js` for a local dev server. Server will listen on port 8080 for http requests coming from  `http://localhost:4200/`. The backend will restart automatically after source files were changed. Make sure you have a .env file with valid DB connection parameters in your server repo.

## Database Connection
The Backend needs a Postgres Database that was set up with the DDL script in the database repo and that was also populated with data.

make sure you have a .env file in the server repo that contains valid DB connection settings in the following format:

```
POSTGRES_DB_DIALECT=postgres
POSTGRES_DB_CONNECTION_STRING=jdbc:postgresql://<<<hostname>>>:<<<port>>>/<<<databasename>>>
POSTGRES_DB_DATABASE=<<databasename>>
POSTGRES_DB_USER=<<databaseUsername>>
POSTGRES_DB_PW=<<password>>>
POSTGRES_DB_HOST=<<hostname>>>
```
##Deployment
The backend was configured to be deployed by generating a docker file and pushing it to Amazon ECR where a task with the updated docker file can be started in the cluster environment configured with Amazon ECS.

In order to deploy the backend server:

1. Make sure that your local .env file points to the production database. The env-file is not part of the repository and the local version will be copied  into the docker image during the deployment process. Navigate with your CLI to the root of your server repo.

2. Log into the AWS CLI with an account that has sufficient privileges for the ECS production environment.
3. Retrieve the AWS ECR login via the AWS CLI:<br/> 
`aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 724078274750.dkr.ecr.us-east-2.amazonaws.com/graphdemobackend`

4. Create the new Docker file:<br />
`docker build -t graphdemobackend .`

5. After the build completes, tag your image so you can push the image to the ECR repository:<br/>
docker tag graphdemobackend:latest 724078274750.dkr.ecr.us-east-2.amazonaws.com/graphdemobackend:latest`

6. Run the following command to push this image to the ECR repository:<br />
`docker push 724078274750.dkr.ecr.us-east-2.amazonaws.com/graphdemobackend:latest`

7. If the task with the old docker image keeps running in ECS, stop the old task and create a new task in the AWS management console so that the updated docker image is used.


