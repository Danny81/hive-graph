const NODE_DEGREE_THRESHOLD = 5; // TODO: make node filter value user adjustable
const LINK_VALUE_THRESHOLD = 10000; // TODO: make link filter value user adjustable

const ELLIPSIS = '…';
const NUM_FORMAT = d3.format(',.3f');
const HEADER_HEIGHT = d3.select('header').node().getBoundingClientRect().height;
const main = d3.select('main');
const chartFilter = d3.select('#chartFilter');
const nodeDegreeValue = d3.select('#nodeDegreeValue').text(getNumberString(NODE_DEGREE_THRESHOLD));
const linkValueThresholdValue = d3.select('#linkValueThresholdValue').text(getNumberString(LINK_VALUE_THRESHOLD));
const nodesValue = d3.select('#nodesValue').text('…');
const edgesValue = d3.select('#edgesValue').text('…');
const timeToRenderValue = d3.select('#timeToRenderValue').text('…');
const simulationDurationValue = d3.select('#simulationDurationValue').text('…');
const svg = d3.select('svg');
const [, , width, height] = svg.attr('viewBox').split(' ');
const svgContent = svg.append('g')
  .classed('content', true)
  .call(
    d3.drag()
      .on('drag', () => {
        const { dx, dy } = d3.event;
        const prevX = +svgContent.attr('data-x') || 0;
        const prevY = +svgContent.attr('data-y') || 0;
        const x = prevX + dx;
        const y = prevY + dy;
        svgContent
          .attr('data-x', x)
          .attr('data-y', y)
          .attr('transform', `translate(${x},${y})`);
      }),
  );

const contentDrag = svgContent.append('rect')
  .classed('content-drag', true)
  .attr('x', 0)
  .attr('y', 0)
  .attr('width', width)
  .attr('height', height);

const graphContainer = svgContent.append('g')
  .classed('graph-container', true);

window.addEventListener('load', updateSvgSize);
window.addEventListener('resize', updateSvgSize);

function updateSvgSize() {
  const rect = main.node().getBoundingClientRect();
  svg.attr('width', rect.width)
    .attr('height', rect.height - HEADER_HEIGHT);
}

let nodeDegreeThreshold = NODE_DEGREE_THRESHOLD;
let linkValueThreshold = LINK_VALUE_THRESHOLD;
chartFilter.select('#btnSubmit')
  .on('click', () => {
    nodeDegreeThreshold = getNumericFormValue(chartFilter.select('#nodeDegreeThreshold'), nodeDegreeThreshold);
    linkValueThreshold = getNumericFormValue(chartFilter.select('#linkValueThreshold'), linkValueThreshold);
    draw();
  });

function getNumericFormValue(selection, defaultValue) {
  const value = +selection.node().value;
  return isNaN(value) || value <= 0 ? defaultValue : value;
}

const color = d3.scaleOrdinal(d3.schemeCategory20);

const simulation = d3.forceSimulation()
  .force('link', d3.forceLink().id(d => d.id))
  .force('charge', d3.forceManyBody().distanceMin(30).distanceMax(500).strength(-500))
  .force('center', d3.forceCenter(width / 2, height / 2))
  // .force('collision', d3.forceCollide().radius(function(d) { return d.r }))
  // .force('x', d3.forceX(width / 2))
  // .force('y', d3.forceY(height / 2))
;

let graph;
d3.json('data/voest.json', (e, g) => {
  if (e) {
    throw e;
  }

  graph = { nodes: [], links: [] };

  // Rectify links
  for (const link of g.links) {
    const value = +link.value;
    if (!isNaN(value)) {
      link.value = value;
      graph.links.push(link);
    }
  }

  // Rectify nodes (by degree) and remove unused links
  for (const node of g.nodes) {
    const degree = +node.degree;
    if (isNaN(degree)) {
      graph.links = graph.links.filter(l => l.source !== node.id && l.target !== node.id);
    } else {
      node.degree = degree;
      graph.nodes.push(node);
    }
  }

  // Ensure all remaining nodes still have at least one link
  graph.nodes = graph.nodes.filter(node =>
    graph.links.some(link => link.source === node.id || link.target === node.id));

  console.log('graph', graph);

  draw();
});

function draw() {
  graphContainer.html('');

  nodeDegreeValue.text(nodeDegreeThreshold);
  linkValueThresholdValue.text(linkValueThreshold);
  edgesValue.text(ELLIPSIS);
  nodesValue.text(ELLIPSIS);
  timeToRenderValue.text(ELLIPSIS);
  simulationDurationValue.text(ELLIPSIS);

  const startTime = moment();

  // Step 1. Filter links by value
  let graphLinks = graph.links.filter(l => l.value >= linkValueThreshold);

  // Step 2. Filter nodes by degree
  let graphNodes = graph.nodes.filter(n => {
    if (n.degree < nodeDegreeThreshold) {
      graphLinks = graphLinks.filter(l => l.source !== n.id && l.target !== n.id);
      return false;
    }
    return true;
  });

  // Step 3. Filter nodes without connections
  graphNodes = graphNodes.filter(n => graphLinks.some(l => l.source === n.id || l.target === n.id));

  // Step 4. Filter links without nodes
  graphLinks = graphLinks.filter(l => graphNodes.some(n => n.id === l.source || n.id === l.target));

  console.log(
    'draw',
    'config',
    {
      nodeDegree: nodeDegreeThreshold,
      linkValue: linkValueThreshold,
    },
    'graph',
    { nodes: graphNodes, links: graphLinks },
  );

  const link = graphContainer.append('g')
    .attr('class', 'links')
    .selectAll('line')
    .data(graphLinks)
    .enter()
    .append('line')
    .attr('stroke-width', d => Math.sqrt(d.value / 1000.0 + 0.25));

  const node = graphContainer.append('g')
    .attr('class', 'nodes')
    .selectAll('circle')
    .data(graphNodes)
    .enter()
    .append('circle')
    .attr('r', 10)
    .attr('fill', d => color(d.group))
    .on('click', onNodeClick)
    .call(
      d3.drag()
        .on('start', onNodeDragStart)
        .on('drag', onNodeDrag)
        .on('end', onNodeDragEnd),
    );

  node.append('title')
    .text(d => d.name + ' (' + d.id + ') ' + d.group);

  simulation
    .nodes(graphNodes)
    .on('tick', onSimulationTick)
    .on('end', onSimulationEnd)
    .force('link')
    .links(graphLinks);

  function onSimulationTick() {
    link
      .attr('x1', d => d.source.x)
      .attr('y1', d => d.source.y)
      .attr('x2', d => d.target.x)
      .attr('y2', d => d.target.y);

    node
      .attr('cx', d => d.x)
      .attr('cy', d => d.y)
      .attr('r', d => 4 + d.degree / 25);
  }

  function onSimulationEnd() {
    if (ELLIPSIS === simulationDurationValue.text()) {
      simulationDurationValue.text(getDurationString(startTime, moment()));
    }
  }

  timeToRenderValue.text(getDurationString(startTime, moment()));

  edgesValue.text(getNumberString(graphLinks.length));
  nodesValue.text(getNumberString(graphNodes.length));

  const nodesValueListElem = nodesValue.node().parentNode;
  if (graphNodes.length <= 0) {
    nodesValueListElem.classList.add('warning');
  } else {
    nodesValueListElem.classList.remove('warning');
  }
}

function getNumberString(n) {
  return NUM_FORMAT(n).replace(/\.0+$/, '');
}

function getDurationString(start, end) {
  const ms = end.diff(start);
  let value;
  let unit;
  if (ms < 1000) {
    value = ms;
    unit = 'ms';
  } else {
    value = ms / 1000;
    unit = 's';
  }

  return `${getNumberString(value)} ${unit}`;
}

function onNodeDragStart(d) {
  if (!d3.event.active) {
    simulation.alphaTarget(0.3).restart();
  }

  d.fx = d.x;
  d.fy = d.y;
}

function onNodeDrag(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function onNodeDragEnd(d) {
  if (!d3.event.active) {
    simulation.alphaTarget(0.0);
  }

  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function onNodeClick(d) {
  console.log('clicked node', d);
  if (!d3.event.active) {
    simulation.alphaTarget(0.0);
  }

  d.fx = undefined;
  d.fy = undefined;
}
