import { DecimalPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompetitorFormComponent } from './component/competitor-form/competitor-form.component';
import { GraphComponent } from './component/graph/graph.component';
import { HeaderComponent } from './component/header/header.component';
import { IndustryFilterComponent } from './component/industry-filter/industry-filter.component';
import { LinkDetailsComponent } from './component/link-details/link-details.component';
import { LoadingComponent } from './component/loading/loading.component';
import { MainComponent } from './component/main/main.component';
import { NodeDetailsComponent } from './component/node-details/node-details.component';
import { SankeyComponent } from './component/sankey/sankey.component';
import { ThresholdFormComponent } from './component/threshold-form/threshold-form.component';
import { LongPressDirective } from './directive/long-press.directive';
import { MaterialModule } from './material/material.module';
import { MainPageComponent } from './page/main-page/main-page.component';
import { CustomNumberPipe } from './pipe/custom-number.pipe';
import { GraphMetricsComponent } from './component/graph-metrics/graph-metrics.component';
import { DbadminComponent } from './page/dbadmin/dbadmin.component';
import { MatGridListModule, MatRadioModule, MatStepperModule } from '@angular/material';
import {MatSlideToggleModule } from '@angular/material/slide-toggle';
import { TrackerControllerComponent } from './component/tracker-controller/tracker-controller.component';
import { DetailsOnDemandComponent } from './component/details-on-demand/details-on-demand.component';
import { TabletContentComponent } from './component/tablet-content/tablet-content.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule,
    NgMultiSelectDropDownModule,
    FormsModule,
    MatStepperModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatGridListModule,
  ],
  declarations: [
    AppComponent,
    MainPageComponent,
    HeaderComponent,
    MainComponent,
    GraphComponent,
    IndustryFilterComponent,
    ThresholdFormComponent,
    LoadingComponent,
    NodeDetailsComponent,
    CustomNumberPipe,
    LinkDetailsComponent,
    CompetitorFormComponent,
    SankeyComponent,
    LongPressDirective,
    GraphMetricsComponent,
    DbadminComponent,
    TrackerControllerComponent,
    DetailsOnDemandComponent,
    DetailsOnDemandComponent,
    TabletContentComponent,
  ],
  providers: [DecimalPipe],
  bootstrap: [AppComponent],
  entryComponents: [TabletContentComponent],
})
export class AppModule {
}


