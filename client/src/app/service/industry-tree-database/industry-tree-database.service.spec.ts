import { TestBed } from '@angular/core/testing';

import { IndustryTreeDatabaseService } from './industry-tree-database.service';

describe('IndustryTreeDatabaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IndustryTreeDatabaseService = TestBed.get(IndustryTreeDatabaseService);
    expect(service).toBeTruthy();
  });
});
