import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { BehaviorSubject } from 'rxjs';
import { IndustryTree } from '../../model/industry-tree';
import { TreeNode } from '../../model/tree-node';

@Injectable({
  providedIn: 'root',
})
export class IndustryTreeDatabaseService {
  public readonly nodeChange = new BehaviorSubject<TreeNode[]>([]);

  public get nodes(): TreeNode[] {
    return this.nodeChange.value;
  }

  public setData(industries: IndustryTree): void {
    const data = this.buildFileTree(industries);
    this.nodeChange.next(TreeNode.root(data));
  }

  private buildFileTree(tree: { [key: string]: any }, level = 0, parentNode?: TreeNode): TreeNode[] {
    return Array.from(Object.keys(tree))
      .filter(key => key !== '_color')
      .reduce<TreeNode[]>((accumulator, key) => {
        const value = tree[key];
        const node = new TreeNode();
        node.item = key;

        if (level === 0) {
          node.background = value._color;
        } else if (parentNode) {
          node.background = d3.hsl(parentNode.background).brighter(0.2).toString();
        }

        // if (node.background) {
        //   const color = d3.hsl(node.background);
        //   const s = Math.floor(color.s * 1000) / 1000;
        //   const l = Math.floor(color.l * 1000) / 1000;
        //   node.color = s >= 0.7105 || l >= 0.65 ? '#000' : '#FFF';
        // }

        // node.background = level === 0 ? value._color : (parentNode ? parentNode.background : undefined);
        // node.color = node.background ? (d3.hsl(node.background).l > 0.5 ? '#FFF' : '#000'): undefined;

        if (value !== undefined) {
          if (level <= 2) {
            node.children = this.buildFileTree(value, level + 1, node);
          } else {
            node.item = key;
            node.gicsSubIndustryCode = value;
          }
        }

        return accumulator.concat(node);
      }, [])
      .sort((a, b) => a.item.replace(/^\W+/, 'z').localeCompare(b.item.replace(/^\W+/, 'z')));
  }
}
