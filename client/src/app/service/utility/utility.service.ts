import { Injectable } from '@angular/core';
import { FormControl, ValidatorFn } from '@angular/forms';
import { ScreenCoordinate } from '../../model/device-tracking';

@Injectable({
  providedIn: 'root',
})
export class UtilityService {

  public decodeUriObject<T>(obj: string): T {
    return typeof obj !== 'undefined' && obj !== null ? JSON.parse(atob(decodeURIComponent(obj))) : undefined;
  }

  public encodeUriObject<T>(obj: T): string {
    return encodeURIComponent(btoa(JSON.stringify(obj)));
  }

  public isInteger(value: number): boolean {
    return value - Math.floor(value) === 0;
  }

  public intValidator(): ValidatorFn {
    return (control: FormControl): (null | { appInteger: string }) => {
      const value = Number(control.value);
      return isNaN(value) || !this.isInteger(value) ? { appInteger: `${value} is not an integer` } : null;
    };
  }

  public numericValidator(): ValidatorFn {
    return (control: FormControl): (null | { appNumeric: string }) => {
      const value = Number(control.value);
      return isNaN(value) ? { appNumeric: `${value} is not a number` } : null;
    };
  }

  public getScreenCoordinates(obj): ScreenCoordinate {
    const p = {x: obj.offsetLeft * window.devicePixelRatio, y: obj.offsetTop * window.devicePixelRatio};
    while (obj.offsetParent) {
      p.x = p.x + obj.offsetParent.offsetLeft * window.devicePixelRatio;
      p.y = p.y + obj.offsetParent.offsetTop * window.devicePixelRatio;
      if (obj === document.getElementsByTagName('body')[0]) {
        break;
      } else {
        obj = obj.offsetParent;
      }
    }
    return p;
  }


}
