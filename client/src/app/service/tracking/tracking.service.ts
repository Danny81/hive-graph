import { Injectable } from '@angular/core';
import { TrackingMessage, TrackingMessageTypes } from '../../model/device-tracking';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  private readonly serverSocketIP: string;
  private readonly serverSocketPort: number;

  private socket: WebSocket;
  private messageQueue: TrackingMessage[];
  public deviceID: string;

  public showNodeDetails = new Subject<TrackingMessage>();
  public transferVisualizationToWallDisplay = new Subject<TrackingMessage>();
  public displayContentOnWallDisplay = new Subject<TrackingMessage>();

  constructor() {

    // TODO read from config
    this.serverSocketIP = '10.21.9.60';
    this.serverSocketPort = 2112;
    this.messageQueue = [];
  }

  public InitConnection(): void {

    this.socket =  new WebSocket(`ws://${this.serverSocketIP}:${this.serverSocketPort}/trackinggateway`);

    this.socket.addEventListener('open', ()  => { this.Handshake(); });
    this.socket.addEventListener('message', () => {this.OnMessageReceived(event); });

  }

  private AddToMessageQueue(msg: TrackingMessage) {
    const messagesInQueue = this.messageQueue.push(msg);
    console.log(`[tracking-service] now ${messagesInQueue} messages in queue`);
  }d

  private HandleMessageQueue() {
    console.log('[tracking-service] HandleMessageQueue...');
    while (this.messageQueue.length > 0) {
      console.log('[tracking-service] next tem in queue: ' + JSON.stringify(this.messageQueue[0]));
      this.socket.send(JSON.stringify(this.messageQueue[0]));
      this.messageQueue.shift();
    }

  }

  public Handshake(): void {
    console.log('[tracking-service] connection now open...');
    const msg = JSON.parse(`{"messageType": "${TrackingMessageTypes.InitConnection}"}`);
    this.socket.send(JSON.stringify(msg));
    this.HandleMessageQueue();

  }

  public OnMessageReceived(event): void {
    console.log('[tracking-service] message received: ' + event.data);

    try {
      const msg: TrackingMessage = JSON.parse(event.data);
      if (msg.messageType === TrackingMessageTypes.ShowNodeDetails) {
        console.log('[tracking-service] have to show node-details...');
        this.showNodeDetails.next(msg);

      } else if (msg.messageType === TrackingMessageTypes.RequestTabletContent) {
        console.log('[tracking-service] have to send my visualization to wall display...');
        this.transferVisualizationToWallDisplay.next(msg);
      } else if (msg.messageType === TrackingMessageTypes.SendTabletContent && this.deviceID === 'WallDisplay') {
        console.log('[tracking-service]: received new content for wall screen ');
        this.displayContentOnWallDisplay.next(msg);
      }
    } catch (e) {
      console.log('[tracking-service] no JSON data');
    }

  }


  public SendMessageToSocket(msg: TrackingMessage): void  {
    this.AddToMessageQueue(msg);
    if (this.socket.readyState === WebSocket.OPEN) {
      this.HandleMessageQueue();
    } else {
      console.warn('[tracking-service] socket not open - message will be parked in queue...');
    }
  }

}
