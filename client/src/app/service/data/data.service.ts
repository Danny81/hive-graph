import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AvailableFilters } from '../../model/available-filters';
import { IndustryTree } from '../../model/industry-tree';
import { ILinkDetails } from '../../model/link-details';
import { Network, NetworkNode } from '../../model/network';
import { NetworkFilter, NetworkFilterSerializable } from '../../model/network-filter';
import { INodeDetails } from '../../model/node-details';
import { Threshold } from '../../model/threshold';

@Injectable({
  providedIn: 'root',
})
export class DataService {

  public constructor(private readonly _httpClient: HttpClient) {
  }

  public getAvailableFilters(): Observable<AvailableFilters> {
    return this._httpClient.get<AvailableFilters>(this.buildUrl('network/available-filters'));
  }

  public getAllGicsSubindustryCodes(tree: IndustryTree): Set<string> {
    const result = new Set<string>();
    if (tree) {
      Object.values(tree).forEach(l1 => {
        Object.values(l1).forEach(l2 => {
          Object.values(l2).forEach(l3 => {
            Object.values(l3).forEach(gic => result.add(gic));
          });
        });
      });
    }

    return result;
  }

  public serializableFilter(filter: NetworkFilter): NetworkFilterSerializable {
    return filter
      ? {
        ...filter,
        selectedSubIndustries: Array.from(filter.selectedSubIndustries.values()),
      }
      : undefined;
  }

  public unserializableFilter(filter: NetworkFilterSerializable): NetworkFilter {
    return filter
      ? {
        egoId: filter.egoId,
        egoDepth: filter.egoDepth,
        originNodes: filter.originNodes,
        originDepths: filter.originDepths,
        competitorToken: filter.competitorToken,
        selectedSubIndustries: new Set<string>(filter.selectedSubIndustries),
        nodesThreshold: Threshold.init(filter.nodesThreshold),
        linksThreshold: Threshold.init(filter.linksThreshold),
        calculateMetrics: filter.calculateMetrics,
        showLabels: filter.showLabels,
        anonymize: filter.anonymize,
        selectedCompetitorIDs: filter.selectedCompetitorIDs,
        selectedCompetitorNames: filter.selectedCompetitorNames,
      }
      : undefined;
  }

  public loadNetwork(filter?: NetworkFilter): Observable<Network> {
    return this.loadNetwork2(this.serializableFilter(filter));
  }

  public loadNetwork2(filter?: NetworkFilterSerializable): Observable<Network> {
    return this._httpClient.post<Network>(this.buildUrl('network'), filter);
  }

  public getNextXTiers(id: string, depth: string, filter?: NetworkFilter): Observable<Network> {
      return this.getNextXTiers2(id, depth, this.serializableFilter(filter));
  }

  public getNextXTiers2(id: string, depth: string, filter?: NetworkFilterSerializable): Observable<Network> {
    return this._httpClient.post<Network>(
      this.buildUrl(`network/${encodeURIComponent(id)}/${encodeURIComponent(depth)}`),
      filter,
    );
  }

  public getCompetitors(node: string, anonymize: string): Observable<INodeDetails[]> {
    return this._httpClient.get<INodeDetails[]>(this.buildUrl(`network/nodes/competitors/${encodeURIComponent(node)}/${encodeURIComponent(anonymize)}`));
  }

  public getNode(id: string, filter?: NetworkFilter): Observable<INodeDetails> {
    return this._httpClient.post<INodeDetails>(
      this.buildUrl(`network/nodes/${encodeURIComponent(id)}`),
      this.serializableFilter(filter),
    );
  }

  public getNetworkNode(id: string, filter?: NetworkFilter): Observable<NetworkNode> {
    return this._httpClient.post<NetworkNode>(
      this.buildUrl(`network/nodes/${encodeURIComponent(id)}`),
      this.serializableFilter(filter),
    );
  }

  public getLink(id: string, anonymize: string): Observable<ILinkDetails> {
    return this._httpClient.get<ILinkDetails>(this.buildUrl(`network/links/${encodeURIComponent(id)}/${encodeURIComponent(anonymize)}`));
  }

  public addMeasurement(interaction: string, id: string, detectionTime: number,  completionTime: number,  elapsedTime: number): Observable<object> {
    return this._httpClient.get(this.buildUrl(`metrics/add/${encodeURIComponent(interaction)}/${encodeURIComponent(id)}/${encodeURIComponent(String(detectionTime))}/${encodeURIComponent(String(completionTime))}/${encodeURIComponent(String(elapsedTime))}`));
  }

  private buildUrl(path: string): string {
    return `${environment.server.host}:${environment.server.port}/rest/${path}`;
  }

  // Maintenance
  public deleteIndustries(): Observable<object> {
    const url = this.buildUrl('industries/maintenance/deleteall');
    return this._httpClient.get(url);
  }

  public deleteLinks(): Observable<object> {
    const url = this.buildUrl('network/maintenance/links/deleteall');
    return this._httpClient.get(url);
  }

  public deleteNodes(): Observable<object> {
    const url = this.buildUrl('network/maintenance/nodes/deleteall');
    return this._httpClient.get(url);
  }

  public importIndustries(industries: object): Observable<object> {
    const url = this.buildUrl('industries/maintenance/import');
    return this._httpClient.post<object>(url, industries);
  }
  public importNodes(nodes: object): Observable<object> {
    const url = this.buildUrl('network/maintenance/nodes/import');
    return this._httpClient.post<object>(url, nodes);
  }

  public importLinks(links: object): Observable<object> {
    const url = this.buildUrl('network/maintenance/links/import');
    return this._httpClient.post<object>(url, links);
  }

}
