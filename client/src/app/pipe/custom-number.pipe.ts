import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customNumber',
})
export class CustomNumberPipe implements PipeTransform {

  public constructor(private readonly _decimalPipe: DecimalPipe) {
  }

  public transform(value: number, format = '1.0-2', currency?: 'USD' | 'EUR'): string {
    let truncatedValue: number;
    let suffix: string;
    if (value >= 1_000_000_000_000) {
      truncatedValue = value / 1_000_000_000_000;
      suffix = ' tn';
    } else if (value >= 1_000_000_000) {
      truncatedValue = value / 1_000_000_000;
      suffix = ' bn';
    } else if (value >= 1_000_000) {
      truncatedValue = value / 1_000_000;
      suffix = ' m';
    } else if (value >= 1_000) {
      truncatedValue = value / 1_000;
      suffix = ' k';
    } else {
      truncatedValue = value;
      suffix = '';
    }

    let result = this._decimalPipe.transform(truncatedValue, format) + suffix;
    if (currency) {
      result = `${(currency === 'USD' ? '$' : '€')} ${result}`;
    }

    return result;
  }

}
