// export type IndustryTree = Map<string, Map<string, Map<string, Map<string, string>>>>
export interface IndustryTree {
  readonly [key: string]: {
    readonly [key: string]: {
      readonly [key: string]: string
    }
  };
}
