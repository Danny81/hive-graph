export interface Range {
  readonly min: number;
  readonly max: number;
}
