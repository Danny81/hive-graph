import { IPosition } from './node-click-event';

export interface TrackingMessage {

  fromDeviceID?: string;
  toDeviceID?: string;
  messageID: string;
  messageType: TrackingMessageTypes;
  nodeWallDisplayPressedAttributes?: NodeWallDisplayPressedAttributes;
  showNodeDetailsAttributes?: ShowNodeDetailsAttributes;
  showTabletContentAttributes?: MessageAttributesShowTabletContentAttributes;
  requestTabletContentAttributes?: MessageAttributesRequestTabletContent;
}

export interface NodeWallDisplayPressedAttributes {

  screenResolutionX: number;
  screenResolutionY: number;
  devicePixelRatio: number;
  nodeId: string;
  readonly x: number;
  readonly y: number;
  readonly xAbsolute: number;
  readonly yAbsolute: number;
  graphComponentPosition: ScreenCoordinate;
  detectionTime: Date;

}

export interface ShowNodeDetailsAttributes {
  nodeID: string;
  detectionTime: number;
}

export interface MessageAttributesShowTabletContentAttributes {
  dataUrl: string;
  position?: IPosition;
}

export interface ScreenCoordinate {

  x: number;
  y: number;
}

export interface MessageAttributesRequestTabletContent {
  relativePosOnScreenX: number;
  relativePosOnScreenY: number;
  detectedTime: number;
}

export enum TrackingMessageTypes {
  InitConnection,
  Identify,
  Deregister,
  NodePressedWallDisplay,
  ShowNodeDetails,
  RequestTabletContent,
  SendTabletContent,
  ProjectTabletContent
}

