import { NetworkLink, NetworkNode } from './network';

export interface IPosition {
  left: number;
  right: number;
  top: number;
  bottom: number;
  width: number;
  height: number;
  center: {
    x: number;
    y: number;
  };
}

export interface INodeClickEvent {
  node: NetworkNode;
  position?: IPosition;
}

export interface ILinkClickEvent {
  link: NetworkLink;
  position?: IPosition;
}
