export interface ClickedNode {
  id: string;
  clicks: number;
}

export interface ClickedNodes {
   nodes: {
     [key: string]: ClickedNode
   };
}
