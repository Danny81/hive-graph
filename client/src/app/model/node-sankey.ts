import { ILinkDetails } from './link-details';
import { INodeDetails } from './node-details';

export interface INodeSankeyNodeDetails extends INodeDetails {
  readonly originalId?: string;
}

export class NodeSankey {
  public nodes: INodeSankeyNodeDetails[];
  public links: ILinkDetails[];
}
