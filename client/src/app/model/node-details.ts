import { SankeyNodeMinimal } from 'd3-sankey';

import { ILinkDetails } from './link-details';
import { NodeSankey } from './node-sankey';

export interface INodeDetails extends SankeyNodeMinimal<INodeDetails, ILinkDetails> {
  readonly id: string;
  name: string;
  readonly country: string;
  readonly gicsSubIndustryCode: number;
  readonly sector: string;
  readonly industryGroup: string;
  readonly industry: string;
  readonly subIndustry: string;
  readonly cogs: number;
  readonly capex: number;
  readonly financeIn: number;
  readonly financeOut: number;
  degree: number;
  eigenCentrality: number;
  betweennessCentrality: number;
  readonly competitorToken: number;
  readonly color: string;
  readonly sankey?: NodeSankey;
  detectionTime?: number;
}
