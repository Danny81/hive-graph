export class FlatTreeNode {
  public item: string;
  public gicsSubIndustryCode?: string;
  public level: number;
  public expandable: boolean;
  public background?: string;
  public color?: string;
}
