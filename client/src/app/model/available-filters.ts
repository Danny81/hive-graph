import { IndustryTree } from './industry-tree';
import { Range } from './range';

export interface AvailableFilters {
  readonly industryTree: IndustryTree;
  readonly nodes: Range;
  readonly links: Range;
}
