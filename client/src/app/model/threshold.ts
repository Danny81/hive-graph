export enum ThresholdType {
  Min = 'Min',
  Max = 'Max',
  Between = 'Between'
}

export interface IThreshold {
  readonly type: string;
  readonly min?: number;
  readonly max?: number;
}

export abstract class Threshold implements IThreshold {

  public abstract readonly type: ThresholdType;

  public static init(template: IThreshold): Threshold {
    if (!template) {
      return undefined;
    }

    switch (template.type) {
      case ThresholdType.Min:
        return new MinThreshold(template.min);
      case ThresholdType.Max:
        return new MaxThreshold(template.max);
      case ThresholdType.Between:
        return new BetweenThreshold(template.min, template.max);
      default:
        return undefined;
    }
  }

  protected constructor(
    public readonly min?: number,
    public readonly max?: number,
  ) {
  }

  public equals(other: Threshold): boolean {
    return other && this.type === other.type && this.min === other.min && this.max === other.max;
  }

  public check(n: number): boolean {
    const hasMin = typeof this.min === 'number';
    const hasMax = typeof this.max === 'number';
    if (hasMin && hasMax) {
      return n >= this.min && n <= this.max;
    } else if (hasMin && !hasMax) {
      return n >= this.min;
    } else if (!hasMin && hasMax) {
      return n <= this.max;
    } else {
      return true;
    }
  }

  public abstract format(unit?: string): string;
}

export class MinThreshold extends Threshold {
  public readonly type = ThresholdType.Min;

  public constructor(min: number) {
    super(min, undefined);
  }

  public format(unit = ''): string {
    return `${this.type}: ${this.min}${unit}`;
  }
}

export class MaxThreshold extends Threshold {
  public readonly type = ThresholdType.Max;

  public constructor(max: number) {
    super(undefined, max);
  }

  public format(unit = ''): string {
    return `${this.type}: ${this.max}${unit}`;
  }
}

export class BetweenThreshold extends Threshold {
  public readonly type = ThresholdType.Between;

  public constructor(min: number, max: number) {
    super(min, max);
  }

  public format(unit = ''): string {
    return `${this.type}: ${this.min}${unit} - ${this.max}${unit}`;
  }
}
