export class TreeNode {
  public static readonly ROOT = 'Select/unselect all';

  public children: TreeNode[];
  public item: string;
  public gicsSubIndustryCode?: string;
  public background?: string;
  public color?: string;

  public static root(treeNodes: TreeNode[]): TreeNode[] {
    const root = new TreeNode();
    root.item = TreeNode.ROOT;
    root.children = treeNodes;
    return [root];
  }
}
