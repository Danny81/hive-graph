import { SankeyGraph } from 'd3-sankey';

import { INodeDetails } from './node-details';

export interface ILinkDetails extends SankeyGraph<INodeDetails, ILinkDetails> {
  readonly id: string;
  readonly source: string;
  readonly sourceDetails: INodeDetails;
  readonly target: string;
  readonly targetDetails: INodeDetails;
  readonly capex: number;
  readonly shareCustomerCogs: number;
  edgeBetweenness: number;

  value: number;
  originalValue?: number;
}
