import { Threshold } from './threshold';

export class NetworkFilter {
  public egoId: string;
  public egoDepth: string;
  public originNodes: string[];
  public originDepths: number[];
  public competitorToken: number;
  public selectedSubIndustries: Set<string>;
  public nodesThreshold: Threshold;
  public linksThreshold: Threshold;
  public calculateMetrics: boolean;
  public showLabels: boolean;
  public anonymize: boolean;
  public selectedCompetitorIDs: string[];
  public selectedCompetitorNames: string[];
}

export class NetworkFilterSerializable {
  public egoId: string;
  public egoDepth: string;
  public originNodes: string[];
  public originDepths: number[];
  public competitorToken: number;
  public selectedSubIndustries: string[];
  public nodesThreshold: Threshold;
  public linksThreshold: Threshold;
  public calculateMetrics: boolean;
  public showLabels: boolean;
  public anonymize: boolean;
  public selectedCompetitorIDs: string[];
  public selectedCompetitorNames: string[];
}
