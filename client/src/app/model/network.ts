import { NetworkFilter } from './network-filter';

export interface NetworkNode extends d3.SimulationNodeDatum {
  readonly id: string;
  name: string;
  readonly group: string;
  readonly color: string;
  readonly degree: number;
  readonly sector?: string;
  readonly industryGroup?: string;
  readonly industry?: string;
  readonly subIndustry?: string;
  readonly gicsSubIndustryCode?: string;
  readonly showlabel?: boolean;
  readonly currentDepth: number;
  readonly betweennessCentrality: number;
  readonly eigenCentrality: number;
  readonly competitorToken: number;
  readonly numeric_id?: number;
  readonly topPopular?: boolean;
  readonly topGateKeeper?: boolean;
  readonly topInfluencer?: boolean;
  r?: number;
}

export interface NetworkLink extends d3.SimulationLinkDatum<NetworkNode> {
  readonly id: string;
  readonly source: NetworkNode;
  readonly target: NetworkNode;
  readonly value: string | number;
  readonly capex: number;
  readonly shareCustomerCogs: number;
  readonly edgeBetweenness?: number;
  readonly showlabel?: boolean;
  readonly topBridge?: boolean;
}

export interface Network {
  readonly filter?: NetworkFilter;
  readonly nodes: NetworkNode[];
  readonly links: NetworkLink[];
}
