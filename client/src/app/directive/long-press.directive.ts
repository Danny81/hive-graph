import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({ selector: '[appLongPress]' })
export class LongPressDirective {

  @Input() public readonly duration = 500;

  @Output() public readonly longPress = new EventEmitter();
  @Output() public readonly longPressing = new EventEmitter();
  @Output() public readonly longPressEnd = new EventEmitter();

  private isPressing: boolean;
  private isLongPressing: boolean;
  private timeout: number;
  private mouseX = 0;
  private mouseY = 0;

  @HostListener('mousedown', ['$event'])
  @HostListener('touchstart', ['$event'])
  public onMouseDown(event): void {
    event.stopPropagation();
    this.startPress(event);
  }

  @HostListener('contextmenu', ['$event'])
  public onContextMenu(event): void {
    event.stopPropagation();
    event.preventDefault();
  }

  @HostListener('mousemove', ['$event'])
  @HostListener('touchmove', ['$event'])
  public onMouseMove(event): void {
    event.stopPropagation();
    this.onPressing(event);
  }

  @HostListener('mouseup', ['$event'])
  @HostListener('touchend', ['$event'])
  public onMouseUp(event): void {
    event.stopPropagation();
    this.endPress();
  }

  private loop(event): void {
    console.log('[long-press] loop', event);
    this.timeout = setTimeout(() => {
      if (this.isLongPressing) {
        this.longPressing.emit(event);
        this.loop(event);
      } else {
        this.endPress();
      }
    }, 50);
  }

  private startPress(event): void {
    console.log('[long-press] start press', event);
    this.mouseX = event.clientX;
    this.mouseY = event.clientY;

    this.isPressing = true;
    this.isLongPressing = false;

    this.timeout = setTimeout(() => {
      console.log('[long-press] activate longpressing');
      this.isLongPressing = true;
      this.longPress.emit(event);
      this.loop(event);
    }, this.duration);
  }

  private onPressing(event): void {
    console.log('[long-press] on pressing', event);
    if (this.isPressing && !this.isLongPressing) {
      const xThreshold = (event.clientX - this.mouseX) > 10;
      const yThreshold = (event.clientY - this.mouseY) > 10;
      if (xThreshold || yThreshold) {
        this.endPress();
      }
    }
  }

  private endPress(): void {
    console.log('[long-press] end press');
    clearTimeout(this.timeout);
    this.isLongPressing = false;
    this.isPressing = false;
    this.longPressEnd.emit(true);
  }

}
