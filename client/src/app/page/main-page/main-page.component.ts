import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, filter, finalize, map, take, takeUntil } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AvailableFilters } from '../../model/available-filters';
import { Network } from '../../model/network';
import { NetworkFilter, NetworkFilterSerializable } from '../../model/network-filter';
import { Threshold, ThresholdType } from '../../model/threshold';
import { DataService } from '../../service/data/data.service';
import { UtilityService } from '../../service/utility/utility.service';
import { TrackingService } from '../../service/tracking/tracking.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit, OnDestroy {

  public filter: NetworkFilter = {
    egoId: environment.visConfig.ego.id,
    egoDepth: environment.visConfig.ego.egoDepth,
    originNodes: [environment.visConfig.ego.id],
    originDepths: [parseInt(environment.visConfig.ego.egoDepth, 10)],
    competitorToken: environment.visConfig.ego.initCompetitorToken,
    selectedSubIndustries: new Set<string>(),
    nodesThreshold: undefined,
    linksThreshold: undefined,
    calculateMetrics: true,
    showLabels: true,
    anonymize: false,
    selectedCompetitorIDs: [],
    selectedCompetitorNames: []
  };
  public availableFilters: AvailableFilters;
  public network: Network;
  public isLoadingAvailableFilters = true;
  public isLoadingNetwork = true;

  private readonly _ngDestroy = new Subject<void>();
  private readonly _nextFilter = new Subject<NetworkFilter>();
  private readonly _nextReload = new Subject<void>();

  public constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _data: DataService,
    private readonly _utility: UtilityService,
  ) {
    this.onFilterChange = this.onFilterChange.bind(this);
    this.updateUrl = this.updateUrl.bind(this);
    this.reload = this.reload.bind(this);
  }

  public ngOnInit(): void {
    this._nextFilter
      .pipe(takeUntil(this._ngDestroy), debounceTime(200))
      .subscribe(this.updateUrl);

    this._data.getAvailableFilters()
      .pipe(takeUntil(this._ngDestroy), take(1), finalize(() => this.isLoadingAvailableFilters = false))
      .subscribe(
        async filters => {
          this.availableFilters = filters;

          const queryParams = this._activatedRoute.snapshot.queryParams;
          if (!queryParams || !queryParams.filter) {
            let requireUrlUpdate = false;
            let defaultNodesThreshold: Threshold;
            let defaultLinksThreshold: Threshold;
            let defaultIndustrySelection: Set<string>;

            if (!this.filter.nodesThreshold || !this.filter.linksThreshold) {
              defaultNodesThreshold =
                this.filter.nodesThreshold || Threshold.init({
                  type: ThresholdType.Min,
                  min: this.availableFilters.nodes.min,
                });

              defaultLinksThreshold = this.filter.linksThreshold || Threshold.init({
                type: ThresholdType.Min,
                min: this.availableFilters.links.min,
              });

              requireUrlUpdate = true;
            }

            const selectedSubIndustries = this.filter.selectedSubIndustries;
            if (!selectedSubIndustries || selectedSubIndustries.size <= 0) {
              defaultIndustrySelection = this._data.getAllGicsSubindustryCodes(this.availableFilters.industryTree);
              requireUrlUpdate = true;
            }

            if (requireUrlUpdate) {
              await this.updateUrl({
                ...this.filter,
                nodesThreshold: defaultNodesThreshold || this.filter.nodesThreshold,
                linksThreshold: defaultLinksThreshold || this.filter.linksThreshold,
                selectedSubIndustries: defaultIndustrySelection || selectedSubIndustries,
              });
            }
          }

          this._activatedRoute.queryParams
            .pipe(
              takeUntil(this._ngDestroy),
              filter(p => !!p.filter),
              map(p => this._utility.decodeUriObject<NetworkFilterSerializable>(p.filter)),
              map(f => this._data.unserializableFilter(f)),
            )
            .subscribe(this.reload);

          console.info('[main-page] Filters', this.availableFilters);
        },
        e => console.error('[main-page] Could not get industry tree', e),
      );
  }

  public ngOnDestroy(): void {
    this._ngDestroy.next();
  }

  public onFilterChange(networkFilter: NetworkFilter): void {
    this._nextFilter.next(networkFilter);
  }

  private async updateUrl(networkFilter: NetworkFilter): Promise<void> {
    const serializableFilter = this._data.serializableFilter(networkFilter);

    console.debug('[main] Update URL', serializableFilter);

    await this._router.navigate([], {
      relativeTo: this._activatedRoute,
      queryParams: { filter: this._utility.encodeUriObject(serializableFilter) },
      queryParamsHandling: 'merge',
    });
  }

  private reload(networkFilter?: NetworkFilter): void {
    if (!networkFilter) {
      return;
    }

    this._nextReload.next();
    this.isLoadingNetwork = true;
    this.filter = networkFilter;
    // this._data.loadNetwork(networkFilter)
    this._data.getNextXTiers(this.filter.egoId, this.filter.egoDepth, networkFilter)
      .pipe(
        takeUntil(this._ngDestroy),
        takeUntil(this._nextReload),
        take(1),
        finalize(() => this.isLoadingNetwork = false),
      )
      .subscribe(
        network => {
          this.network = network;
          console.info('[main-page] Network', this.network);
        },
        e => console.error('[main-page] Could not get filtered network', e),
      );
  }
}
