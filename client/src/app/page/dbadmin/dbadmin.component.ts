import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../service/data/data.service';
import * as Papa from 'papaparse';
import { take } from 'rxjs/operators';

/**
 * @title Stepper overview
 */
@Component({
  selector: 'app-dbadmin',
  templateUrl: 'dbadmin.component.html',
  styleUrls: ['dbadmin.component.scss'],
})
export class DbadminComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  resultFromDB: string;
  resetMode: string;
  modes: string[] = ['All', 'Nodes/Links', 'Links', 'Nothing'];
  dataList: any[];
  errorOcurred: boolean;
  importing: boolean;

  @ViewChild('uploadIndustries', {static: false} ) uploadIndustries: ElementRef;
  @ViewChild('uploadNodes', {static: false} ) uploadNodes: ElementRef;
  @ViewChild('uploadLinks', {static: false} ) uploadLinks: ElementRef;

  public constructor(
    private _formBuilder: FormBuilder,
    private readonly _data: DataService
  ) {
    this.resultFromDB = '';
    this.errorOcurred = false;
    this.importing = false;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ['', Validators.required]
    });
  }

  onDeleteClicked($event: MouseEvent) {
    this.resultFromDB = '';
    if (this.resetMode === 'All' || this.resetMode === 'Links' || this.resetMode === 'Nodes/Links') {
      const res = this._data.deleteLinks().pipe().subscribe(
        result => {
          if (typeof result === 'number') {
            this.resultFromDB = this.resultFromDB.concat(result, ' Links deleted \n');
          } else {
            this.resultFromDB = this.resultFromDB.concat(JSON.stringify(result, undefined, 4), '\n');
            this.errorOcurred = true;
          }
        },
        e => {
          console.error('[dbadmin] could not delete links', e);
          this.resultFromDB = 'Could not delete links';
        },
      );
    }

    if (this.resetMode === 'All' || this.resetMode === 'Nodes/Links') {

        const sepp = this._data.deleteNodes().pipe().subscribe(
          result => {
            if (typeof result === 'number') {
              this.resultFromDB = this.resultFromDB.concat(result, ' Nodes deleted \n');
            } else {
              this.resultFromDB = this.resultFromDB.concat(JSON.stringify(result, undefined, 4), '\n');
              this.errorOcurred = true;
            }
          },
          e => {
            console.error('[dbadmin] could not delete nodes', e);
            this.resultFromDB = 'Could not delete nodes';
          },
        );

    }

    if (this.resetMode === 'All') {

      const sepp = this._data.deleteIndustries().pipe().subscribe(
        result => {
          if (typeof result === 'number') {
            this.resultFromDB = this.resultFromDB.concat(result, ' Industries deleted \n');
          } else {
            this.resultFromDB = this.resultFromDB.concat(JSON.stringify(result, undefined, 4), '\n');
            this.errorOcurred = true;
          }
        },
        e => {
          console.error('[dbadmin] could not delete industries', e);
          this.resultFromDB = 'Could not delete industries';
        },
      );

    }

    this.resultFromDB = this.resultFromDB.replace(new RegExp('\n', 'g'), '<br />');

  }

  uploadListener(files: File[]) {
    const text = [];

    if (files[0]) {
      console.log(files[0]);
      Papa.parse(files[0], {
        header: true,
        skipEmptyLines: true,
        complete: (result, file) => {
          console.log(result);
          this.dataList = result.data;
        }
      });
    }
  }

   processImportResponseOk(result: object, dataType: string) {
    if (typeof result === 'boolean') {
      if (result) {
        this.errorOcurred = false;
        this.resultFromDB = dataType + ' successfully imported';
      }
    } else {
      this.resultFromDB = this.resultFromDB.concat(JSON.stringify(result, undefined, 4), '\n');
      this.errorOcurred = true;
    }
    this.importing = false;
  }

  processImportResponseErr(e: object, dataType: string) {
    console.error('[dbadmin] could not import ' + dataType, e);
    this.resultFromDB = 'Could not import ' + dataType;
    this.importing = false;
  }

  importData(dataType: string) {
    let res;

    this.resultFromDB = 'Importing...';
    this.importing = true;

    switch (dataType) {
      case 'Industries':
        res = this._data.importIndustries(this.dataList).pipe( take(1)).subscribe(
          result => {this.processImportResponseOk(result, dataType); }, e => {this.processImportResponseErr(e, dataType); });
        break;
      case 'Nodes':
        res = this._data.importNodes(this.dataList).pipe( take(1)).subscribe(
          result => {this.processImportResponseOk(result, dataType); }, e => {this.processImportResponseErr(e, dataType); });
        break;
      case 'Links':
        res = this._data.importLinks(this.dataList).pipe( take(1)).subscribe(
          result => {this.processImportResponseOk(result, dataType); }, e => {this.processImportResponseErr(e, dataType); });
        break;
    }
  }

  resetForNextStep() {
    this.resultFromDB = '';
    this.importing = false;
    this.errorOcurred = null;
    this.dataList = null;
    this.uploadIndustries.nativeElement.value = null;
    this.uploadNodes.nativeElement.value = null;
    this.uploadLinks.nativeElement.value = null;
  }
}
