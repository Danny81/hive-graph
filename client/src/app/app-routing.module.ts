import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './page/main-page/main-page.component';
import { DbadminComponent } from './page/dbadmin/dbadmin.component';

const routes: Routes = [
  {
    path: 'admin',
    pathMatch: 'full',
    component: DbadminComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    component: MainPageComponent,
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
