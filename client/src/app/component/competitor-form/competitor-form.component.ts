import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { ClickedNodes } from '../../model/clicked-nodes';
import { NetworkFilter } from '../../model/network-filter';
import { INodeDetails } from '../../model/node-details';
import { DataService } from '../../service/data/data.service';

@Component({
  selector: 'app-competitor-form',
  templateUrl: './competitor-form.component.html',
  styleUrls: ['./competitor-form.component.scss'],
})
export class CompetitorFormComponent implements OnInit {

  @Input() public dropdownList = [];
  @Input() public nodes: INodeDetails[];
  @Input() public filterValue: NetworkFilter;
  @Input() public clickedNodes: ClickedNodes;

  @Output() public readonly competitorAddedEvent = new EventEmitter<string>();
  @Output() public readonly competitorRemovedEvent = new EventEmitter<string>();

  public selectedItems = [];
  public dropdownSettings = {};

  private readonly _ngDestroy = new Subject<void>();

  constructor(private readonly _data: DataService) {
  }

  ngOnInit() {
    console.log('[competitor-form] ego ', this.filterValue.egoId);
    this._data.getCompetitors(this.filterValue.egoId, String(this.filterValue.anonymize)).pipe(takeUntil(this._ngDestroy))
      .subscribe(
        nodes => {
          this.nodes = nodes;
          this.dropdownList = this.nodes;
          console.info('[competitor-form] nodes', this.nodes);
        },
        e => console.error('[competitor-form] Could not get competitors', e),
      );

    for (let i = 0; i < this.filterValue.selectedCompetitorIDs.length; i++) {
      this.selectedItems.push({id: this.filterValue.selectedCompetitorIDs[i], name: this.filterValue.selectedCompetitorNames[i] });
    }

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: false,
      maxHeight: 600,
    };
  }

  onItemSelect(item: any) {
    console.log(item);
    this.handleCompetitorSelected(item);
  }

  private handleCompetitorSelected(item: any) {
    if (this.clickedNodes && this.clickedNodes.nodes[item.id] === undefined) {
      console.info('[competitor-form] node not in click-collection');
      this.clickedNodes.nodes[item.id] = { id: item.id, clicks: 1 };
    } else {
      console.info('[competitor-form] node IN click-collection');
      if (this.clickedNodes.nodes[item.id].clicks <= 0) {
        this.clickedNodes.nodes[item.id].clicks = 1;
      }
    }
    this.competitorAddedEvent.emit(item.id);
  }

  private handleCompetitorDeselected(item: any) {
    console.log(item);
    this.clickedNodes.nodes[item.id].clicks = 0;
    this.competitorRemovedEvent.emit(item.id);
  }

  onDeSelect(item: any) {
    console.log(item);
    this.clickedNodes.nodes[item.id].clicks = 0;
    this.competitorRemovedEvent.emit(item.id);
  }

  onSelectAll(items: any) {
    for (const item of items.length) {
      this.handleCompetitorSelected(item);
    }
  }

  onDeSelectAll(items: any) {
    console.log('all deselected');
    for (const item of this.dropdownList) {
      this.handleCompetitorDeselected(item);
    }
  }

  onFilterChanged($event) {

  }
}
