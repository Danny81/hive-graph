import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import * as d3 from 'd3';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Network, NetworkLink, NetworkNode } from '../../model/network';
import { NetworkFilter } from '../../model/network-filter';
import { ILinkClickEvent, INodeClickEvent, IPosition } from '../../model/node-click-event';


@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
})
export class GraphComponent implements OnInit, OnDestroy {

  @Output() public readonly nodeClicked = new EventEmitter<INodeClickEvent>();
  @Output() public readonly linkClicked = new EventEmitter<ILinkClickEvent>();

  private readonly _ngDestroy = new Subject<void>();
  private readonly _config = new BehaviorSubject<{ network?: Network, filter?: NetworkFilter }>({});
  private readonly _egoConfig = environment.visConfig.ego;
  private readonly _competitionConfig = environment.visConfig.competition;
  private readonly _nodeConfig = environment.visConfig.node;

  private _main: d3.Selection<HTMLElement, {}, HTMLElement, void>;
  private _svg: d3.Selection<SVGElement, {}, HTMLElement, void>;
  private _height: number;
  private _width: number;
  private _headerHeight: number;
  private _simulation: d3.Simulation<NetworkNode, NetworkLink>;
  private _graphContainer: d3.Selection<SVGGElement, {}, HTMLElement, void>;

  public constructor(private readonly _elementRef: ElementRef) {
    this.onNodeClick = this.onNodeClick.bind(this);
    this.onLinkClick = this.onLinkClick.bind(this);
    this.onNodeDragStart = this.onNodeDragStart.bind(this);
    this.onNodeDrag = this.onNodeDrag.bind(this);
    this.onNodeDragEnd = this.onNodeDragEnd.bind(this);
  }

  @Input()
  public set network(network: Network) {
    console.info('[vis] Draw %d nodes with %d links', network.nodes.length, network.links.length);
    this._config.next({ ...this._config.value, network });
  }

  @Input()
  public set filter(networkFilter: NetworkFilter) {
    console.info('[vis] Update filter', networkFilter);
    this._config.next({ ...this._config.value, filter: networkFilter });
  }

  public ngOnInit(): void {
    const elementRefSelection = d3.select(this._elementRef.nativeElement);
    this._main = elementRefSelection.select('.app-vis');
    this._svg = elementRefSelection.select('svg');
    const [, , width, height] = this._svg.attr('viewBox').split(' ');
    this._width = +width;
    this._height = +height;
    this._headerHeight = (d3.select('header').node() as any).getBoundingClientRect().height;

    const svgContent = this._svg.append('g')
      .classed('content', true)
      .call(d3.drag().on('drag', () => svgContent.attr('transform', d3.event.transform)));

    this._graphContainer = svgContent;

    this._svg.call(d3.zoom().on('zoom', () => svgContent.attr('transform', d3.event.transform)));

    this._simulation = d3.forceSimulation<NetworkNode, NetworkLink>()
      .force('link', d3.forceLink<NetworkNode, NetworkLink>().id(d => d.id))
      .force('charge', d3.forceManyBody().distanceMin(50).distanceMax(500).strength(-500))
      .force('center', d3.forceCenter(this._width / 2, this._height / 2))
      .force('collision', d3.forceCollide().radius((d: NetworkNode) => d.r))
      .force('x', d3.forceX(this._width / 2))
      .force('y', d3.forceY(this._height / 2)
      );

    this._config
      .pipe(takeUntil(this._ngDestroy), filter(config => !!config.network && !!config.filter))
      .subscribe(config => this.draw(config.network, config.filter));
  }

  public ngOnDestroy(): void {
    this._ngDestroy.next();
  }

  private draw(graph: { nodes: NetworkNode[], links: NetworkLink[] }, visFilter: NetworkFilter): void {
    console.info('[vis] Draw graph', graph);

    this._graphContainer.html('');

    const self = this;
    const link = this._graphContainer.append('g')
      .classed('app-links', true)
      .selectAll('.app-links')
      .data(graph.links)
      .enter()
      .append('line')
      .classed('topBridge', d => d.topBridge === true)
      .classed('app-highlight', d => d.source.id === visFilter.egoId || d.target.id === visFilter.egoId)
      .attr('stroke-width', (d: NetworkLink) => Math.sqrt(d.topBridge ? 200 : (!isNaN(+d.value) ? +d.value : 1) / 1000.0 + 0.25))
      .on('click', function(d: NetworkLink) {
        self.onLinkClick(this, d);
        link.classed('app-selected-link', nd => nd === d);
      });

    const node = this._graphContainer.append('g')
      .classed('app-nodes', true)
      .selectAll('.app-node')
      .data(graph.nodes)
      .enter()
      .append('circle')
      .classed('app-node', true)
      .classed('app-selected-node', false)
      .classed('app-highlight', d => d.id === visFilter.egoId)
      .on('click', function(d: NetworkNode) {
        self.onNodeClick(this, d);
        node.classed('app-selected-node', nd => nd === d);
      })
      .call(
        d3.drag()
          .on('start', this.onNodeDragStart)
          .on('drag', this.onNodeDrag)
          .on('end', this.onNodeDragEnd),
      )
      .attr('r', (d: NetworkNode) => {
        if (d.id === this._egoConfig.id || d.id === visFilter.egoId) {
          d.r = this._egoConfig.minRadius;
        } else {
          d.r = this._nodeConfig.minRadius;
        }
        d.r += (!isNaN(+d.degree) ? +d.degree / 25 : 1) * this._nodeConfig.scaleFactor;
        return d.r;
      })
      .attr('fill', (d: NetworkNode) => d.color);

    node.append('title')
      .text((d: NetworkNode) => `${d.name} \n${d.sector}: ${d.subIndustry}`);

    const nodeLabel = this._graphContainer.append('g')
      .classed('app-node-labels', true)
      .selectAll('.app-node-label')
      .data(graph.nodes.filter(n => n.showlabel ))
      .enter()
      .append('text')
      .classed('app-node-label', true)
      .text(d => d.name)
      .style('font-size', '45px')
      .style('font-weight', 'bold');

    (this._simulation
      .nodes(graph.nodes)
      .on('tick', onSimulationTick)
      // .on('end', onSimulationEnd)
      .force('link') as d3.ForceLink<any, any>)
      .links(graph.links);

    function onSimulationTick() {
      node
        .attr('cx', (d: NetworkNode) => d.x)
        .attr('cy', (d: NetworkNode) => d.y);

      nodeLabel
        .attr('x', (d: NetworkNode) => d.x)
        .attr('y', (d: NetworkNode) => d.y);

      link
        .attr('x1', (d: NetworkLink) => (d.source as NetworkNode).x)
        .attr('y1', (d: NetworkLink) => (d.source as NetworkNode).y)
        .attr('x2', (d: NetworkLink) => (d.target as NetworkNode).x)
        .attr('y2', (d: NetworkLink) => (d.target as NetworkNode).y);
    }

    function onSimulationEnd() {
      console.info('[vis] Simulation end');
    }
  }

  private onNodeDragStart(d: NetworkNode): void {
    if (!d3.event.active) {
      this._simulation.alphaTarget(0.3).restart();
    }

    d.fx = d.x;
    d.fy = d.y;
  }

  private onNodeDrag(d: NetworkNode): void {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  private onNodeDragEnd(d: NetworkNode): void {
    if (!d3.event.active) {
      this._simulation.alphaTarget(0.0);
    }

    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  private onNodeClick(element: SVGElement, d: NetworkNode): void {
    console.info('[vis] Clicked node', d);
    if (!d3.event.active) {
      this._simulation.alphaTarget(0.0);
    }

    d.fx = undefined;
    d.fy = undefined;

    const svgBounds = this._svg.node().getBoundingClientRect();
    const bounds = element.getBoundingClientRect();
    const left = bounds.left - svgBounds.left;
    const top = bounds.top - svgBounds.top;
    const position: IPosition = {
      left,
      right: left + bounds.width,
      top,
      bottom: top + bounds.height,
      width: bounds.width,
      height: bounds.height,
      center: {
        x: left + (bounds.width / 2),
        y: top + (bounds.height / 2),
      },
    };

    this.nodeClicked.emit({ position, node: d });
  }

  private onLinkClick(element: SVGElement, d: NetworkLink): void {
    console.info('[vis] Clicked link', d, d3.event);
    if (!d3.event.active) {
      this._simulation.alphaTarget(0.0);
    }

    this.linkClicked.emit({ link: d });
  }
}
