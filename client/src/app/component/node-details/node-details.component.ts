import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ClickedNode, ClickedNodes } from '../../model/clicked-nodes';
import { NetworkNode } from '../../model/network';
import { INodeDetails } from '../../model/node-details';

@Component({
  selector: 'app-node-details',
  templateUrl: './node-details.component.html',
  styleUrls: ['./node-details.component.scss'],
})
export class NodeDetailsComponent implements OnInit, OnChanges {
  @Input() public readonly reloading = false;
  @Input() public readonly node: INodeDetails;
  @Input() public readonly currentEgoNode: NetworkNode;
  @Input() public clickedNodes: ClickedNodes;
  @Output() public readonly largeChange = new EventEmitter<boolean>();
  @Output() public readonly hide = new EventEmitter<void>();
  @Output() public readonly expandCollapseEvent = new EventEmitter<string>();
  @Output() public readonly nodeClicked = new EventEmitter<INodeDetails>();
  @Output() public readonly changeEgoNode = new EventEmitter<string>();

  private _large = false;
  private _clickedNode: ClickedNode;

  @Input()
  public get large(): boolean {
    return this._large;
  }

  public set large(isLarge: boolean) {
    this._large = isLarge;
    this.largeChange.emit(isLarge);
  }

  public ngOnInit(): void {
    this._clickedNode = this.clickedNodes.nodes[this.node.id];
  }

  public ngOnChanges(changes: SimpleChanges): void {
    console.log('[node details] changes', changes);
  }

  public toggleLarge(event: MouseEvent): void {
    event.stopPropagation();
    this.large = !this._large;
  }

  public onCollapseClicked(event: MouseEvent): void {
    event.stopPropagation();
    if (this._clickedNode) {
      this._clickedNode.clicks--;
    }

    if (this.node) {
      this.expandCollapseEvent.emit(this.node.id);
    }
  }

  public onExpandClicked(event: MouseEvent): void {
    event.stopPropagation();
    if (this._clickedNode) {
      this._clickedNode.clicks++;
    }

    if (this.node) {
      this.expandCollapseEvent.emit(this.node.id);
    }
  }

  public collapseDisabled(): boolean {
    return this._clickedNode && this._clickedNode.clicks <= 0;
  }

  public expandDisabled(): boolean {
    return this._clickedNode && this._clickedNode.clicks >= 4;
  }

  public onSetAsEgoNodeClicked(event: MouseEvent) {
    event.stopPropagation();
    console.log('[node-details] ego node change: ' + this.node.name);
    this.changeEgoNode.emit(this.node.name);
  }

  public setAsEgoNodeDisabled(): boolean {
    return false;
  }

  public close(event: MouseEvent) {
    event.stopPropagation();
    this.hide.emit();
  }
}
