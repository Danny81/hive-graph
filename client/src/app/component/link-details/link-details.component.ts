import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ILinkDetails } from '../../model/link-details';
import { INodeDetails } from '../../model/node-details';

@Component({
  selector: 'app-link-details',
  templateUrl: './link-details.component.html',
  styleUrls: ['./link-details.component.scss'],
})
export class LinkDetailsComponent {
  @Input() public readonly reloading = false;
  @Input() public readonly link: ILinkDetails;
  @Output() public readonly hide = new EventEmitter<void>();

  public industryCaption(node: INodeDetails): string {
    let caption = node.sector;
    if (node.industryGroup !== node.sector) {
      caption += `, ${node.industryGroup}`;
    }

    if (node.industry !== node.industryGroup) {
      caption += `, ${node.industry}`;
    }

    if (node.subIndustry !== node.industry) {
      caption += `, ${node.subIndustry}`;
    }

    return caption;
  }
}
