import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletContentComponent } from './tablet-content.component';

describe('TabletContentComponent', () => {
  let component: TabletContentComponent;
  let fixture: ComponentFixture<TabletContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabletContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabletContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
