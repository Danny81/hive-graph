import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { TrackingMessage } from '../../model/device-tracking';

@Component({
  selector: 'app-tablet-content',
  templateUrl: './tablet-content.component.html',
  styleUrls: ['./tablet-content.component.scss']
})
export class TabletContentComponent implements OnInit, OnChanges {

  @Input() public readonly content: TrackingMessage;
  @Output() public readonly hideTabletContent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  public close(event: MouseEvent) {
    event.stopPropagation();
    this.hideTabletContent.emit(this.content.messageID);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('[tablet-content] changes', changes);
  }

}
