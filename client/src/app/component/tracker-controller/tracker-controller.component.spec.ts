import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackerControllerComponent } from './tracker-controller.component';

describe('TrackerControllerComponent', () => {
  let component: TrackerControllerComponent;
  let fixture: ComponentFixture<TrackerControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackerControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackerControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
