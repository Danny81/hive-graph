import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatRadioChange, MatSelectChange } from '@angular/material';
import { TrackingMessage, TrackingMessageTypes } from '../../model/device-tracking';
import { TrackingService } from '../../service/tracking/tracking.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-tracker-controller',
  templateUrl: './tracker-controller.component.html',
  styleUrls: ['./tracker-controller.component.scss']
})
export class TrackerControllerComponent implements OnInit {

  deviceTypes: string[] = ['Wall Display', 'Tablet'];
  @Output() public selectedDeviceType: string;
  public previousDeviceType: string;
  public selectedDeviceTypeInternal: string;
  @Output() public selectedTracker: string;
  public selectedTrackerInternal: string;
  public previousDeviceTracker: string;

  @Output() public readonly sendTrackingMessageEvent = new EventEmitter<TrackingMessage>();

  constructor(private trackingService: TrackingService) {

  }

  ngOnInit() {
    this.selectedDeviceType = localStorage.getItem('SelectedDeviceType');
    this.selectedDeviceTypeInternal = localStorage.getItem('SelectedDeviceType');
    this.previousDeviceType = localStorage.getItem('PreviousDeviceType');
    this.selectedTracker = localStorage.getItem('SelectedTracker');
    this.selectedTrackerInternal = localStorage.getItem('SelectedTracker');
    this.previousDeviceTracker = localStorage.getItem('previousDeviceTracker');

    if (this.selectedTracker  && this.selectedDeviceType === 'Tablet') {
      const msg = { fromDeviceID: this.selectedTracker, messageID: uuidv4(), messageType: TrackingMessageTypes.Identify };
      this.trackingService.deviceID = this.selectedTracker;
      this.sendTrackingMessageEvent.emit(msg);
    } else if (this.selectedDeviceType === 'Wall Display') {
      const msg = { fromDeviceID: 'WallDisplay', messageID: uuidv4(), messageType: TrackingMessageTypes.Identify };
      this.trackingService.deviceID = 'WallDisplay';
      this.sendTrackingMessageEvent.emit(msg);
    }
  }

  OnDeviceTypeChanged(event: MatRadioChange) {
    console.log('[tracker-controller] device type selection changed: ' + event.value);
    this.previousDeviceType = this.selectedDeviceTypeInternal;
    this.selectedDeviceTypeInternal = event.value;


    localStorage.setItem('SelectedDeviceType', this.selectedDeviceTypeInternal );
    localStorage.setItem('PreviousDeviceType', this.previousDeviceType);

    if ( this.previousDeviceType === 'Wall Display' && this.selectedDeviceTypeInternal === 'Tablet' ) {
      console.log('[tracker-controller] switched from Wall Display to Wal Tablet mode - resetting Wall Display Registration...');
      this.sendTrackingMessageEvent.emit({ fromDeviceID: 'WallDisplay', messageID: uuidv4(),
        messageType: TrackingMessageTypes.Deregister });
    } else
    if ( this.previousDeviceType === 'Tablet' && this.selectedDeviceTypeInternal === 'Wall Display' ) {
      console.log('[tracker-controller] switched from Tablet to Wal Display mode - resetting Tracker Registration...');
      localStorage.removeItem('SelectedTracker');
      localStorage.removeItem('previousDeviceTracker');

      if ( this.selectedTracker ) {
        this.sendTrackingMessageEvent.emit( { fromDeviceID: this.selectedTracker, messageID: uuidv4(),
          messageType: TrackingMessageTypes.Deregister });
      }

      this.selectedTrackerInternal = undefined;
      this.previousDeviceTracker  = undefined;
      this.selectedTracker = undefined;

      const msg = { fromDeviceID: 'WallDisplay', messageID: uuidv4(), messageType: TrackingMessageTypes.Identify };
      this.trackingService.deviceID = 'WallDisplay';
      this.sendTrackingMessageEvent.emit(msg);

    }

  }

  onTrackerSelectionChanged($event: MatSelectChange) {
    console.log('[tracker-controller] tracker selection changed:');
    if ($event.value !== undefined) {
      this.previousDeviceTracker = this.selectedTrackerInternal;
      this.selectedTrackerInternal = $event.value;
    } else {
      this.previousDeviceTracker = this.selectedTrackerInternal === undefined ? undefined : this.selectedTrackerInternal.toString();
      this.selectedTrackerInternal = $event.value;
    }
    console.log('[tracker-controller] new value of selectedTracker property: ' + this.selectedTrackerInternal);
    console.log('[tracker-controller] new value of previousDeviceTracker property: ' + this.previousDeviceTracker);
    localStorage.setItem('SelectedTracker', $event.value);
    localStorage.setItem('previousDeviceTracker', this.previousDeviceTracker);
    if ($event.value !== undefined) {
      const msg = { fromDeviceID: $event.value, messageID: uuidv4(), messageType: TrackingMessageTypes.Identify };
      this.trackingService.deviceID =  $event.value;
      this.sendTrackingMessageEvent.emit(msg);
    } else {
      if (this.previousDeviceTracker !== undefined) {
        const msg = { fromDeviceID: this.previousDeviceTracker, messageID: uuidv4(), messageType: TrackingMessageTypes.Deregister };
        console.log(msg);
        this.sendTrackingMessageEvent.emit(msg);
      }
    }
  }
}
