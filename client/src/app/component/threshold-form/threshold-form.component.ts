import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Range } from '../../model/range';
import { BetweenThreshold, MaxThreshold, MinThreshold, Threshold, ThresholdType } from '../../model/threshold';
import { UtilityService } from '../../service/utility/utility.service';

@Component({
  selector: 'app-threshold-form',
  templateUrl: './threshold-form.component.html',
  styleUrls: ['./threshold-form.component.scss'],
})
export class ThresholdFormComponent implements OnInit, OnDestroy {

  @Input() public readonly range: Range;
  @Input() public readonly step = 1;

  @Output() public readonly thresholdChange = new EventEmitter<Threshold>();

  public readonly modes: ThresholdType[] = [ThresholdType.Min, ThresholdType.Max, ThresholdType.Between];
  public readonly formGroup: FormGroup;

  private readonly _ngDestroy = new Subject<void>();

  private currentThreshold: Threshold;

  public constructor(formBuilder: FormBuilder, private readonly utility: UtilityService) {
    this.onValueChanged = this.onValueChanged.bind(this);

    this.formGroup = formBuilder.group({
      mode: [this.modes[0], Validators.required],
      min: 0,
      max: 0,
    });
  }

  @Input()
  public set threshold(threshold: Threshold) {
    this.currentThreshold = threshold || new MinThreshold(this.range.min);
    this.formGroup.setValue({
      mode: this.currentThreshold.type,
      min: this.currentThreshold.min || (this.range ? this.range.min : 0),
      max: this.currentThreshold.max || (this.range ? this.range.min : 0),
    });
  }

  public ngOnInit(): void {
    this.onValueChanged(this.formGroup.value);
    this.formGroup.valueChanges
      .pipe(takeUntil(this._ngDestroy))
      .subscribe(this.onValueChanged);
  }

  public ngOnDestroy(): void {
    this._ngDestroy.next();
  }

  private get numericValidator(): ValidatorFn {
    return this.utility.isInteger(this.step)
      ? this.utility.intValidator()
      : this.utility.numericValidator();
  }

  private onValueChanged(value: { mode: ThresholdType, min: number, max: number }): void {
    if (isNaN(value.min) || isNaN(value.max)) {
      return;
    }

    const { min, max } = value;
    let threshold: Threshold;
    switch (value.mode) {
      case ThresholdType.Min:
        this.formGroup.controls.min.setValidators([
          Validators.required,
          this.numericValidator,
          Validators.min(this.range.min),
          Validators.max(this.range.max),
        ]);
        this.formGroup.controls.max.clearValidators();
        threshold = new MinThreshold(min);
        break;
      case ThresholdType.Max:
        this.formGroup.controls.min.clearValidators();
        this.formGroup.controls.max.setValidators([
          Validators.required,
          this.numericValidator,
          Validators.min(this.range.min),
          Validators.max(this.range.max),
        ]);
        threshold = new MaxThreshold(max);
        break;
      case ThresholdType.Between:
      default:
        this.formGroup.controls.min.setValidators([
          Validators.required,
          this.numericValidator,
          Validators.min(this.range.min),
          Validators.max(max),
        ]);
        this.formGroup.controls.max.setValidators([
          Validators.required,
          this.numericValidator,
          Validators.min(min),
          Validators.max(this.range.max),
        ]);
        threshold = new BetweenThreshold(min, max);
        break;
    }

    if (!threshold.equals(this.currentThreshold)) {
      this.currentThreshold = threshold;
      this.thresholdChange.emit(threshold);
    }
  }

  public increase(control: AbstractControl): void {
    const value = Math.round(((+control.value) + (+this.step)) * 1000) / 1000;
    if (value <= this.range.max) {
      control.setValue(value);
    }
  }

  public decrease(control: AbstractControl): void {
    const value = Math.round(((+control.value) - (+this.step)) * 1000) / 1000;
    if (value >= this.range.min) {
      control.setValue(value);
    }
  }

}
