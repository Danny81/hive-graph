import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSlideToggle } from '@angular/material';
import { NetworkFilter } from '../../model/network-filter';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent implements  OnInit {

  calcMetrics  = true;
  showLabels = true;
  anonymize = false;

  @Input() public filterValue: NetworkFilter;

  @Output() public readonly calcMetricsToggleEvent = new EventEmitter<boolean>();
  @Output() public readonly showLabelsToggleEvent = new EventEmitter<boolean>();
  @Output() public readonly anonymizeToggleEvent = new EventEmitter<boolean>();

  ngOnInit() {
    console.log('[header] init...');
    this.calcMetrics = this.filterValue.calculateMetrics;
    this.showLabels = this.filterValue.showLabels;
    this.anonymize = this.filterValue.anonymize;
  }

  toggleCalcMetrics($event) {
    this.calcMetrics = ! this.calcMetrics;
    console.log('calcMetrics now ' + this.calcMetrics);
    this.calcMetricsToggleEvent.emit(this.calcMetrics);
  }

  toggleShowLabels($event: void) {
    this.showLabels = ! this.showLabels;
    console.log('showLabels now ' + this.showLabels);
    this.showLabelsToggleEvent.emit(this.showLabels);
  }

  toggleAnonymize($event: void) {
    this.anonymize = ! this.anonymize;
    console.log('anonymize now ' + this.anonymize);
    this.anonymizeToggleEvent.emit(this.anonymize);
  }
}
