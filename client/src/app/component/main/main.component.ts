import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output, QueryList,
  ViewChild, ViewChildren,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import * as d3 from 'd3';
import { Observable, Subject } from 'rxjs';
import { debounceTime, finalize, map, startWith, take, takeUntil } from 'rxjs/operators';
import { AvailableFilters } from '../../model/available-filters';
import { ClickedNodes } from '../../model/clicked-nodes';
import { ILinkDetails } from '../../model/link-details';
import { Network, NetworkNode } from '../../model/network';
import { NetworkFilter } from '../../model/network-filter';
import { ILinkClickEvent, INodeClickEvent } from '../../model/node-click-event';
import { INodeDetails } from '../../model/node-details';
import { INodeSankeyNodeDetails } from '../../model/node-sankey';
import { BetweenThreshold, MaxThreshold, MinThreshold, Threshold, ThresholdType } from '../../model/threshold';
import { DataService } from '../../service/data/data.service';
import {
  MessageAttributesRequestTabletContent, ShowNodeDetailsAttributes,
  TrackingMessage,
  TrackingMessageTypes,
} from '../../model/device-tracking';
import {TrackingService} from '../../service/tracking/tracking.service';
import { UtilityService } from '../../service/utility/utility.service';
import domtoimage from 'dom-to-image';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() public readonly isLoadingAvailableFilters: boolean;
  @Input() public readonly availableFilters: AvailableFilters;
  @Input() public readonly isLoadingNetwork: boolean;
  @Input() public readonly network: Network;
  @Output() public readonly filterChange = new EventEmitter<NetworkFilter>();
  @Output() public competitors: INodeDetails[];
  @ViewChild('mainContent', { static: false }) public readonly mainContentRef: ElementRef;
  @ViewChild('nodeDetailsContainer', { static: false }) public readonly nodeDetailsContainerRef: ElementRef;
  @ViewChild('linkDetailsContainer', { static: false }) public readonly linkDetailsContainerRef: ElementRef;
  @ViewChildren('tabletContentContainer') public readonly tabletContentContainerRefs: QueryList<ElementRef>;
  @ViewChild('trackerController', { static: false }) trackerControllerRef;

  public readonly egoFormControl: FormControl;

  public filteredEgoOptions: Observable<NetworkNode[]>;
  public nodeDetails: INodeDetails;
  public nodeDetailsOnDemand: INodeDetails;
  public isLoadingNodeDetails = false;
  public isNodeDetailsActive = false;
  public isNodeDetailsLarge = false;
  public linkDetails: ILinkDetails;
  public isLoadingLinkDetails = false;
  public clickedNodes: ClickedNodes;
  public egoNode: NetworkNode;
  public filterValue: NetworkFilter;
  private readonly _ngDestroy = new Subject<void>();

  private drag;

  public showingExtendedNodeDetails = false;
  public tabletContents: Map<string, TrackingMessage>;
  public boundTabletContents: TrackingMessage[];

  private readonly _nextNodeDetails = new Subject<INodeClickEvent>();
  private readonly _nextLinkDetails = new Subject<ILinkClickEvent>();
  private readonly _nextTabletContents = new Subject<TrackingMessage>();

  public constructor(formBuilder: FormBuilder, private readonly _data: DataService, private trackingService: TrackingService,
                     private utilityService: UtilityService) {
    this.reloadNodeDetails = this.reloadNodeDetails.bind(this);
    this.reloadLinkDetails = this.reloadLinkDetails.bind(this);
    this.reloadTabletContents = this.reloadTabletContents.bind(this);
    this.egoFormControl = formBuilder.control(this.filterValue, Validators.required);
    this.tabletContents = new Map<string, TrackingMessage>();
    this.boundTabletContents = [];
  }

  @Input()
  public set filter(filterValue: NetworkFilter) {
    this.filterValue = filterValue;
    if (this.filterValue && this.network && this.network.nodes) {
      this.egoNode = this.network.nodes.find(n => n.id === this.filterValue.egoId);
      if (this.egoNode) {
        const ego = this.egoNode.id;
        if (!this.clickedNodes) {
          this.clickedNodes = {
            nodes: {},
          };
          this.clickedNodes.nodes[ego] = { id: ego, clicks: 1 };
        }

        if (!this.egoFormControl.valid) {
          this.egoFormControl.setValue(this.egoNode);
        }
      }
    }
  }

  public ngOnInit(): void {
    this.filteredEgoOptions = this.egoFormControl.valueChanges.pipe(
      takeUntil(this._ngDestroy),
      startWith<NetworkNode | string>(''),
      map(value => {
        const filterValue = typeof value === 'object' ? value.name : (value || '').trim().toLowerCase();
        return filterValue.length > 0
          ? this.network.nodes.filter(node => node.name.toLowerCase().indexOf(filterValue) >= 0)
          : this.network.nodes.slice();
      }),
      map(nodes => nodes.sort((a, b) => a.name.localeCompare(b.name))),
    );

    let prevEgoId: string;
    this.filterChange
      .pipe(takeUntil(this._ngDestroy))
      .subscribe(filter => {
        if (!this.isNodeDetailsActive && filter.egoId !== prevEgoId) {
          this.reloadNodeDetails(filter.egoId);
          prevEgoId = filter.egoId;
        }
      });

    this._nextNodeDetails
      .pipe(takeUntil(this._ngDestroy), debounceTime(200))
      .subscribe(n => {
        this.reloadNodeDetails(n);
      });

    this._nextLinkDetails
      .pipe(takeUntil(this._ngDestroy), debounceTime(200))
      .subscribe(this.reloadLinkDetails);

    this._nextTabletContents
      .pipe(takeUntil(this._ngDestroy), debounceTime(200))
      .subscribe(this.reloadTabletContents);

    this.trackingService.showNodeDetails.subscribe((data) => {
      console.log(`[main] showNodeDetailsReceived from trackingService: ${data}`);
      console.log(`[main] showExtendedNodeDetails: ${this.showingExtendedNodeDetails}`);
     // TODO buggy
      this.showingExtendedNodeDetails = true;
      this.getDetailsOnDemandNode(data.showNodeDetailsAttributes);

    });

    // let image appear on wall display
    this.trackingService.displayContentOnWallDisplay.subscribe((trMessage) => {
      console.log(`[main] displayContentOnWallDisplay`);
      this.tabletContents.set(trMessage.messageID, trMessage);
      this.refreshTabletContentBinding();
      this._nextTabletContents.next(trMessage);
      console.log(`[main] displayContentOnWallDisplay: now holding ${ this.tabletContents.size} vizualisations in collection...` );
    });

    // send image to wall display
    this.trackingService.transferVisualizationToWallDisplay.subscribe((data) => {
      console.log(`[main] transferVisualizationToWallDisplay`);
      const node = document.getElementById('main-content' );
      domtoimage.toJpeg(node, { quality: 0.9, bgcolor:  '#f7f7f7'}).then((imgData) => {
        const msg = { fromDeviceID: data.toDeviceID, toDeviceID: 'WallDisplay', messageID: uuidv4(),
          messageType: TrackingMessageTypes.SendTabletContent,
          showTabletContentAttributes: { dataUrl: imgData, /*position: { center: { x: 0, y: 0, } }*/ },
          requestTabletContentAttributes: {relativePosOnScreenX: data.requestTabletContentAttributes.relativePosOnScreenX,
            relativePosOnScreenY: data.requestTabletContentAttributes.relativePosOnScreenY, detectedTime: data.requestTabletContentAttributes.detectedTime }};
        this.trackingService.SendMessageToSocket(msg);
      });
    });

    this.trackingService.InitConnection();

  }

  public ngAfterViewInit(): void {
     this.drag = (containerRef: ElementRef) => {
      let xOffset = 0;
      let yOffset = 0;
      return d3.drag()
        .on('start', () => {
          const mainBounds = this.mainContentRef.nativeElement.getBoundingClientRect();
          const selectionBounds = containerRef.nativeElement.getBoundingClientRect();
          xOffset = d3.event.x - selectionBounds.x + mainBounds.x;
          yOffset = d3.event.y - selectionBounds.y + mainBounds.y;
          d3.select(containerRef.nativeElement)
            .style('bottom', null)
            .style('right', null);
        })
        .on('drag', () => {
          d3.select(containerRef.nativeElement)
            .style('left', `${d3.event.x - xOffset}px`)
            .style('top', `${d3.event.y - yOffset}px`);
        });
    };

     d3.select(this.nodeDetailsContainerRef.nativeElement).call(this.drag(this.nodeDetailsContainerRef));
     d3.select(this.linkDetailsContainerRef.nativeElement).call(this.drag(this.linkDetailsContainerRef));

  }

  public ngOnDestroy(): void {
    this._ngDestroy.next();
  }

  public displayNetworkNodeName(networkNode?: NetworkNode | string): string {
    return networkNode ? (typeof networkNode === 'object' ? networkNode.name : networkNode) : '';
  }

  public onEgoOptionSelected(event: MatAutocompleteSelectedEvent): void {
    const node = event.option.value as NetworkNode;
    this.onEgoOptionSelectedInternal(node);
  }

  private onEgoOptionSelectedInternal(node: NetworkNode): void {
    if (!node) {
      console.log('[main] Selected ego option called but no node is present');
      return;
    }

    console.info('[main] Selected ego option', node);
    this.clickedNodes = {
      nodes: {},
    };
    this.filterValue.originNodes = [node.id];
    this.filterValue.originDepths = [1];
    this.clickedNodes.nodes[node.id] = { id: node.id, clicks: 1 };
    this.filterChange.emit({
      ...this.filterValue,
      egoId: node.id,
      competitorToken: node.competitorToken,
      selectedSubIndustries: node.gicsSubIndustryCode
        ? new Set<string>([...Array.from(this.filterValue.selectedSubIndustries.values()), node.gicsSubIndustryCode])
        : this.filterValue.selectedSubIndustries,
    });

    this.onNodeClicked({ position: undefined, node });
    this.reloadCompetitors(node.id);

  }

  public onNodeClicked(event: INodeClickEvent): void {

    const metricTimeStamp = Date.now();

    if (!this.nodeDetails || this.nodeDetails.id !== event.node.id) {
      console.info('[main] Selected node', event);
      this._nextNodeDetails.next(event);

      if (this.clickedNodes && this.clickedNodes.nodes[event.node.id] === undefined) {
        console.info('[main] node not in click-collection');
        this.clickedNodes.nodes[event.node.id] = { id: event.node.id, clicks: 0 };
      } else {
        console.info('[main] node IN click-collection');
      }
    } else {
      this.fixOverlayPosition(event, this.nodeDetailsContainerRef);
    }

    // TODO remove hard-wiring
    if (this.trackerControllerRef.selectedDeviceType === 'Wall Display') {
      console.info('[main] Node Press was on wall display, preparing NodePressedMessage...', event);
      const graphComponent = document.getElementById('graph-container');
      const graphStartPos = this.utilityService.getScreenCoordinates(graphComponent);

      const msg = { fromDeviceID: 'WallDisplay', messageID: uuidv4(), messageType: TrackingMessageTypes.NodePressedWallDisplay,
        NodeWallDisplayPressedAttributes: {
          /*resolution / coordinate calculted in absolute pixels, ruling out diferent screen settings like e.g. 176% scale factor*/
          screenResolutionX: window.screen.width * window.devicePixelRatio,
          screenResolutionY: window.screen.height * window.devicePixelRatio,
          devicePixelRatio: window.devicePixelRatio,
          nodeID: event.node.id,
          x: event.position.center.x * window.devicePixelRatio,
          y: event.position.center.y * window.devicePixelRatio,
          xAbsolute: event.position.center.x * window.devicePixelRatio + graphStartPos.x,
          yAbsolute: event.position.center.y * window.devicePixelRatio + graphStartPos.y,
          graphComponentPosition: graphStartPos,
          detectionTime: metricTimeStamp,
        } };
      this.trackingService.SendMessageToSocket(msg);

    }


    this.isNodeDetailsActive = true;
  }

  public onLinkClicked(event: ILinkClickEvent): void {
    if (!this.linkDetails || this.linkDetails.id !== event.link.id) {
      console.info('[main] Selected link', event);
      this._nextLinkDetails.next(event);
    } else {
      this.fixOverlayPosition(event, this.linkDetailsContainerRef);
    }
  }

  public onFlowNodeClicked(node: INodeSankeyNodeDetails): void {
    console.info('[main] Selected flow node', node);
    this._data.getNode(node.originalId || node.id, this.filterValue)
      .pipe(
        takeUntil(this._ngDestroy),
        take(1),
        finalize(() => this.isLoadingNodeDetails = false),
      )
      .subscribe(details => {
        this.nodeDetails = details;
      });
  }

  public getDetailsOnDemandNode(event: ShowNodeDetailsAttributes): void {

    this._data.getNode( event.nodeID, this.filterValue)
      .pipe(
        takeUntil(this._ngDestroy),
        take(1),
        finalize(() => this.isLoadingNodeDetails = false),
      )
      .subscribe(details => {

        /*if (typeof event !== 'string') {
          details.betweennessCentrality = event.node.betweennessCentrality;
          details.degree = event.node.degree;
          details.eigenCentrality = event.node.eigenCentrality;
        }*/
        this.nodeDetailsOnDemand = details;
        this.nodeDetailsOnDemand.detectionTime = event.detectionTime;
      });

  }

  public reloadNodeDetails(event: string | INodeClickEvent): void {
    this.isLoadingNodeDetails = true;
    if (typeof event !== 'string') {
      this.fixOverlayPosition(event, this.nodeDetailsContainerRef);
    }

    this._data.getNode(typeof event === 'string' ? event : event.node.id, this.filterValue)
      .pipe(
        takeUntil(this._ngDestroy),
        take(1),
        finalize(() => this.isLoadingNodeDetails = false),
      )
      .subscribe(details => {

        if (typeof event !== 'string') {
          details.betweennessCentrality = event.node.betweennessCentrality;
          details.degree = event.node.degree;
          details.eigenCentrality = event.node.eigenCentrality;
        }
        this.nodeDetails = details;

        if (this.nodeDetails && typeof event !== 'string') {
          this.fixOverlayPosition(event, this.nodeDetailsContainerRef);
        }
      });
  }

  public reloadTabletContents(event: TrackingMessage): void {

    let i = 0;
    this.tabletContentContainerRefs.forEach(
      content => {
      d3.select(content.nativeElement).call(this.drag(content));
      // console.log('detectiontime of this item: ' + this.boundTabletContents[i].requestTabletContentAttributes.detectedTime);
      this.fixOverlayPositionTabletContent(this.boundTabletContents[i].requestTabletContentAttributes, content);
      i++;
    });

    const completionTime = Date.now();
    const dStart = new Date(1970, 0, 1);
    const convertedTimeFromUnity = -  this.boundTabletContents[i - 1].requestTabletContentAttributes.detectedTime / 10000;
    const elapsedTime = completionTime -  this.boundTabletContents[i - 1].requestTabletContentAttributes.detectedTime;
    console.log(`[main] time now ${ Date.now()} ms`);
    console.log(`[main] opening details-on-demand took ${elapsedTime} ms`);

    // tslint:disable-next-line:max-line-length
    const sepp = this._data.addMeasurement('TabletToWallScreen', undefined, this.boundTabletContents[i - 1].requestTabletContentAttributes.detectedTime, completionTime, elapsedTime).pipe().subscribe(
      result => {
        console.log(result);
      },
      e => {
        console.error('[main] could not add measurement', e);
      },
    );
    /*for(let i=0; i<this.tabletContentContainerRefs.length; i++) {
      d3.select(this.tabletContentContainerRefs[i].nativeElement).call(this.drag(this.tabletContentContainerRefs[i]));
      this.fixOverlayPosition(event.showTabletContentAttributes, this.tabletContentContainerRefs[i]);
    }*/
  }

  public reloadLinkDetails(event: string | ILinkClickEvent): void {
    this.isLoadingLinkDetails = true;
    if (typeof event !== 'string') {
      this.fixOverlayPosition(event, this.linkDetailsContainerRef);
    }

    this._data.getLink(typeof event === 'string' ? event : event.link.id, this.filterValue.anonymize.toString())
      .pipe(
        takeUntil(this._ngDestroy),
        take(1),
        finalize(() => this.isLoadingLinkDetails = false),
      )
      .subscribe(details => {

        if (typeof event !== 'string') {
          details.edgeBetweenness = event.link.edgeBetweenness;
        }

        this.linkDetails = details;
        if (this.linkDetails && typeof event !== 'string') {
          this.fixOverlayPosition(event, this.linkDetailsContainerRef);
        }
      });
  }

  public onNodesThresholdChanged(nodesThreshold: Threshold): void {
    console.info('[main] Nodes threshold %s', nodesThreshold.format());
    this.filterChange.emit({ ...this.filterValue, nodesThreshold });
  }

  public onLinksThresholdChanged(linksThreshold: Threshold): void {
    let scaledLinksThreshold: Threshold;
    switch (linksThreshold.type) {
      case ThresholdType.Min:
        scaledLinksThreshold = new MinThreshold(linksThreshold.min);
        break;
      case ThresholdType.Max:
        scaledLinksThreshold = new MaxThreshold(linksThreshold.min);
        break;
      case ThresholdType.Between:
      default:
        scaledLinksThreshold = new BetweenThreshold(linksThreshold.min, linksThreshold.max);
        break;
    }

    console.info('[main] Links threshold %s', linksThreshold.format(), scaledLinksThreshold.format());
    this.filterChange.emit({ ...this.filterValue, linksThreshold: scaledLinksThreshold });
  }

  public onIndustrySelectionChanged(selection: Set<string>) {
    console.info('[main] Selected sub-industries', selection);
    this.filterChange.emit({ ...this.filterValue, selectedSubIndustries: selection });
  }

  public expandCollapseEvent(node: string): void {
    console.info('[main] ExpandCollapseEvent received! node: ' + node);
    this.filterValue.egoDepth = this.clickedNodes.nodes[node].clicks.toString();
    const index = this.filterValue.originNodes.indexOf(this.clickedNodes.nodes[node].id);
    if (index <= -1) {
      this.filterValue.originNodes.push(this.clickedNodes.nodes[node].id);
      this.filterValue.originDepths.push(this.clickedNodes.nodes[node].clicks);
    } else {
      this.filterValue.originDepths[index] = this.clickedNodes.nodes[node].clicks;
    }
    this.filterChange.emit(this.filterValue);
  }

  competitorAddedEvent($event: string) {
    this.expandCollapseEvent($event);
    this._data.getNode( $event, this.filterValue)
      .pipe(
        takeUntil(this._ngDestroy),
        take(1),
        finalize(() => this.isLoadingNodeDetails = false),
      )
      .subscribe(details => {
        this.filterValue.selectedCompetitorIDs.push(details.id);
        this.filterValue.selectedCompetitorNames.push((details.name));
        console.log('JUU');
        console.log(this.filterValue.selectedCompetitorIDs.length);
        console.log('ARGH');
      });

  }

  competitorRemovedEvent($event: string) {
    this.expandCollapseEvent($event);
    const index = this.filterValue.selectedCompetitorIDs.indexOf($event);
    if (index > -1) {
      this.filterValue.selectedCompetitorIDs.splice(index, 1);
      this.filterValue.selectedCompetitorNames.splice(index, 1);
    }
  }

  public calcMetricsToggleEvent(value: boolean): void {

    console.info('[main] calcMetricsToggleEvent received! new value: ' + value);
    this.filterValue.calculateMetrics = value;
    this.filterChange.emit(this.filterValue);
  }

  public changeEgoNode(nodeId: string): void {
    this.egoFormControl.setValue(nodeId);
    const node = this.network.nodes.find(n => n.name === nodeId) as NetworkNode;
    this.onEgoOptionSelectedInternal(node);
  }

  public reloadCompetitors(egoNodeId: string): void {
    this._data.getCompetitors(egoNodeId, String(this.filterValue.anonymize))
      .pipe(take(1), takeUntil(this._ngDestroy))
      .subscribe(
        competitors => {
          this.competitors = competitors;
          console.info('[main] ego node ', egoNodeId);
          console.info('[main] competitor nodes', this.competitors);
        },
        e => console.error('[main] Could not get competitors', e),
      );
  }

  private fixOverlayPositionTabletContent(event: MessageAttributesRequestTabletContent,
                                          containerRef: ElementRef):
    void {

    if (!this.mainContentRef || !containerRef) {
      return;
    }
    console.log(`[main] fixOverlayPositionTabletContent: overlay should popup at approximately (x) ${event.relativePosOnScreenX} (y)  ${event.relativePosOnScreenY} portion of the screen`);
    const mainBounds = this.mainContentRef.nativeElement.getBoundingClientRect();
    console.log(mainBounds);
    const screenResolutionX =  window.screen.width * window.devicePixelRatio;
    const screenResolutionY =  window.screen.height * window.devicePixelRatio;
    console.log(`[main] fixOverlayPositionTabletContent: screen resolution is (x): ${screenResolutionX} (y): ${screenResolutionY}`);
    const pointedX = event.relativePosOnScreenX * screenResolutionX;
    const pointedY = event.relativePosOnScreenY * screenResolutionY;
    console.log(`[main] fixOverlayPositionTabletContent: tablet pointed approximately towards (x): ${pointedX} (y): ${pointedY}`);
    const overlaySelection = d3.select(containerRef.nativeElement);
    overlaySelection
      .style('top', `${Math.round((pointedY - mainBounds.y) / window.devicePixelRatio) - 200 }px`)
      .style('left', `${Math.round((pointedX - mainBounds.x) / window.devicePixelRatio) - 200 }px`)
    ;
  }

  private fixOverlayPosition(event: INodeClickEvent | ILinkClickEvent,
                             containerRef: ElementRef):
    void {

    if (!this.mainContentRef || !containerRef) {
      return;
    }
    console.log(event.position);
    const mainBounds = this.mainContentRef.nativeElement.getBoundingClientRect();
    const mainCenter = {
      x: mainBounds.width / 2,
      y: mainBounds.height / 2,
    };

    const overlaySelection = d3.select(containerRef.nativeElement);
    if (event.position) {
      if (event.position.center.x < mainCenter.x) {
        overlaySelection.style('left', `${event.position.right + 20}px`)
          .style('right', null);
      } else {
        overlaySelection.style('right', `${mainBounds.width - event.position.left + 20}px`)
          .style('left', null);
      }

      if (event.position.center.y < mainCenter.y) {
        overlaySelection.style('top', `${event.position.bottom + 20}px`)
          .style('bottom', null);
      } else {
        overlaySelection.style('bottom', `${mainBounds.height - event.position.top + 20}px`)
          .style('top', null);
      }
    } else {
      overlaySelection
        .style('top', '10px')
        .style('left', '10px');
    }
  }

  showLabelsToggleEvent($event: boolean) {
    console.info('[main] showLabelsToggleEvent received! new value: ' + $event);
    this.filterValue.showLabels = $event;
    this.filterChange.emit(this.filterValue);
  }

  anonymizeToggleEvent($event: boolean) {
    console.info('[main] anonymizeToggleEvent received! new value: ' + $event);
    this.filterValue.anonymize = $event;
    this.reloadCompetitors(this.egoNode.id);
    this.filterChange.emit(this.filterValue);

    this._data.getNode( this.egoNode.id, this.filterValue)
        .pipe(
          takeUntil(this._ngDestroy),
          take(1),
          finalize(() => this.isLoadingNodeDetails = false),
        )
        .subscribe(details => {
          this.egoNode.name = details.name;
          this.egoFormControl.setValue(this.egoNode);
        });

  }

  OnSendTrackingMessageEvent($event: TrackingMessage) {
    console.log('[main] OnSendTrackingMessageEvent received...');
    this.trackingService.SendMessageToSocket($event);
  }

  private refreshTabletContentBinding() {
    this.boundTabletContents = Array.from(this.tabletContents.values());
  }

  closeTabletContent($event: string) {
    console.log('[main] closeTabletContent:  closing tablet content wih message id ' + $event);
    this.tabletContents.delete($event);
    this.refreshTabletContentBinding();
  }

  hideDetailsOnDemand($event: void) {
    console.log('[main] HideDetailsOnDemand received...');
    this.nodeDetailsOnDemand = undefined;
    this.showingExtendedNodeDetails = false;
  }
}
