import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FlatTreeNode } from '../../model/flat-tree-node';
import { IndustryTree } from '../../model/industry-tree';
import { TreeNode } from '../../model/tree-node';
import { IndustryTreeDatabaseService } from '../../service/industry-tree-database/industry-tree-database.service';

@Component({
  selector: 'app-industry-filter',
  templateUrl: './industry-filter.component.html',
  styleUrls: ['./industry-filter.component.scss'],
})
export class IndustryFilterComponent implements OnInit, OnDestroy {

  public readonly flatNodeMap = new Map<FlatTreeNode, TreeNode>();
  public readonly nestedNodeMap = new Map<TreeNode, FlatTreeNode>();
  public readonly treeControl: FlatTreeControl<FlatTreeNode>;
  public readonly treeFlattener: MatTreeFlattener<TreeNode, FlatTreeNode>;
  public readonly dataSource: MatTreeFlatDataSource<TreeNode, FlatTreeNode>;
  public readonly checklistSelection = new SelectionModel<FlatTreeNode>(true);

  private readonly _ngDestroy = new Subject<void>();

  private _selectedSubIndustries = new Set<string>();

  public constructor(private readonly _database: IndustryTreeDatabaseService) {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer.bind(this),
      this.getLevel,
      this.isExpandable,
      this.getChildren,
    );
    this.treeControl = new FlatTreeControl<FlatTreeNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  @Input()
  public set industries(industries: IndustryTree) {
    this._database.setData(industries);
  }

  @Input()
  public set selection(selection: Set<string>) {
    this.autoSelect(selection);
  }

  @Output() public readonly selectionChange = new EventEmitter<Set<string>>();

  public getLevel(node: FlatTreeNode) {
    return node.level;
  }

  public isExpandable(node: FlatTreeNode) {
    return node.expandable;
  }

  public getChildren(node: TreeNode): TreeNode[] {
    return node.children;
  }

  public hasChild(_: number, _nodeData: FlatTreeNode) {
    return _nodeData.expandable;
  }

  public ngOnInit(): void {
    this._database.nodeChange
      .pipe(takeUntil(this._ngDestroy))
      .subscribe(data => {
        this.dataSource.data = data;
        this.autoSelect(this._selectedSubIndustries);
      });

    const rootNode = this.dataSource._data.value.find(n => n.item === TreeNode.ROOT);
    if (rootNode) {
      const flatRootNode = this.nestedNodeMap.get(rootNode);
      this.treeControl.expand(flatRootNode);
    }

    this.checklistSelection.changed
      .pipe(takeUntil(this._ngDestroy))
      .subscribe(selection => {
        selection.added.filter(added => !added.expandable)
          .forEach(added => this._selectedSubIndustries.add(added.gicsSubIndustryCode));
        selection.removed.filter(removed => !removed.expandable)
          .forEach(removed => this._selectedSubIndustries.delete(removed.gicsSubIndustryCode));
      });

    if (!this._selectedSubIndustries || this._selectedSubIndustries.size <= 0) {
      this.selectAll();
    }
  }

  public ngOnDestroy(): void {
    this._ngDestroy.next();
  }

  public transformer(node: TreeNode, level: number) {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.item ? existingNode : new FlatTreeNode();
    flatNode.item = node.item;
    flatNode.gicsSubIndustryCode = node.gicsSubIndustryCode;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    flatNode.background = node.background;
    flatNode.color = node.color;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  public descendantsAllSelected(node: FlatTreeNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    return descendants.every(child => this.checklistSelection.isSelected(child));
  }

  public descendantsPartiallySelected(node: FlatTreeNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  public itemSelectionToggle(node: FlatTreeNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    this.emitSelectionChange();
  }

  public leafItemSelectionToggle(node: FlatTreeNode): void {
    this.checklistSelection.toggle(node);
    this.emitSelectionChange();
  }

  private selectAll(): void {
    const selection = new Set<string>();
    for (const node of this.flatNodeMap.keys()) {
      selection.add(node.gicsSubIndustryCode);
      this.checklistSelection.select(node);
    }
    this._selectedSubIndustries = selection;
  }

  private autoSelect(selection: Set<string>): void {
    this._selectedSubIndustries = selection;
    for (const node of this.flatNodeMap.keys()) {
      if (selection.has(node.gicsSubIndustryCode)) {
        this.checklistSelection.select(node);
      } else {
        this.checklistSelection.deselect(node);
      }

      this.checkAllParentsSelection(node);
    }
  }

  private checkAllParentsSelection(node: FlatTreeNode): void {
    let parent: FlatTreeNode | undefined = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  private checkRootNodeSelection(node: FlatTreeNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child => this.checklistSelection.isSelected(child));
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  private getParentNode(node: FlatTreeNode): FlatTreeNode | undefined {
    const currentLevel = this.getLevel(node);
    if (currentLevel < 1) {
      return undefined;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }

    return undefined;
  }

  private emitSelectionChange(): void {
    this._selectedSubIndustries = new Set<string>(this._selectedSubIndustries);
    this.selectionChange.emit(this._selectedSubIndustries);
  }
}
