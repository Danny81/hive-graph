import { Component, Input, OnInit } from '@angular/core';
import { Network, NetworkLink, NetworkNode } from '../../model/network';
import { NetworkFilter } from '../../model/network-filter';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-graph-metrics',
  templateUrl: './graph-metrics.component.html',
  styleUrls: ['./graph-metrics.component.scss']
})
export class GraphMetricsComponent implements OnInit {

  @Input() public network: Network;
  @Input() public filterValue: NetworkFilter;

  private topDegrees: NetworkNode[];
  private topGateKeepers: NetworkNode[];
  private topInfluencers: NetworkNode[];
  private topBridges: NetworkLink[];

  public top3DegreeArray: string[] = [];
  public top3gateKeeperArray: string[] = [];
  public top3influencerArray: string[] = [];
  public top3bridgesArray: string[] = [];

  public topDegreeString = '';
  public topGateKeeperString = '';
  public topInfluencerString = '';
  public topBridgesString = '';

  private readonly maxItemsInPanel = environment.graphMetrics.totalItemsInExpansionPanel;
  private readonly itemsInPanelCollapsed = environment.graphMetrics.numItemsVisibleCollapsed;

  constructor() {
    console.log('[graph-metrics] constructor... ');
  }

  ngOnInit() {
    console.log('[graph-metrics] ' + new Date().toISOString() +  ' init... ');

    for (let i = 0; i < this.itemsInPanelCollapsed; i++ ) {
      this.top3DegreeArray.push('');
      this.top3gateKeeperArray.push('');
      this.top3influencerArray.push('');
      this.top3bridgesArray.push('');
    }

    this.topDegrees  = this.network.nodes.sort((a, b) => (b.topPopular ? 1 : 0) - (a.topPopular ? 1 : 0))
      .sort((a, b) => b.degree - a.degree);
    this.topGateKeepers = this.network.nodes.sort((a, b) => (b.topGateKeeper ? 1 : 0) - (a.topGateKeeper ? 1 : 0))
      .sort((a, b) => b.betweennessCentrality - a.betweennessCentrality);
    this.topInfluencers = this.network.nodes.sort((a, b) => (b.topInfluencer ? 1 : 0) - (a.topInfluencer ? 1 : 0))
      .sort((a, b) => b.eigenCentrality - a.eigenCentrality);
    this.topBridges = this.network.links.sort((a, b) => (b.topBridge ? 1 : 0) - (a.topBridge ? 1 : 0))
      .sort((a, b) => b.edgeBetweenness - a.edgeBetweenness);

    for (let i = 0; i < this.maxItemsInPanel && i < this.topDegrees.length ; i++) {

      if (i < this.itemsInPanelCollapsed) {
        this.top3DegreeArray[i] = (i + 1).toString() + '. ' + this.topDegrees[i].name;
      } else {
        this.topDegreeString = this.topDegreeString.concat((i + 1).toString() + '. ' + this.topDegrees[i].name);
        if (i !== this.topDegrees.length - 1) {
          this.topDegreeString = this.topDegreeString.concat('\n');
        }
      }
    }

    for (let i = 0; i < this.maxItemsInPanel && i < this.topGateKeepers.length ; i++) {

      if (i < this.itemsInPanelCollapsed) {
        this.top3gateKeeperArray[i] = (i + 1).toString() + '. ' + this.topGateKeepers[i].name;
      } else {
        this.topGateKeeperString = this.topGateKeeperString.concat((i + 1).toString() + '. ' + this.topGateKeepers[i].name);
        if (i !== this.topGateKeepers.length - 1) {
          this.topGateKeeperString = this.topGateKeeperString.concat('\n');
        }
      }
    }

    for (let i = 0; i < this.maxItemsInPanel && i < this.topInfluencers.length ; i++) {

      if (i < this.itemsInPanelCollapsed) {
        this.top3influencerArray[i] = (i + 1).toString() + '. ' + this.topInfluencers[i].name;
      } else {
        this.topInfluencerString = this.topInfluencerString.concat((i + 1).toString() + '. ' + this.topInfluencers[i].name);
        if (i !== this.topInfluencers.length - 1) {
          this.topInfluencerString = this.topInfluencerString.concat('\n');
        }
      }
    }

    for (let i = 0; i < this.maxItemsInPanel && i < this.topBridges.length ; i++) {

      // @ts-ignore
      const source = this.network.nodes.find(n => n.id === this.topBridges[i].source);
      // @ts-ignore
      const target = this.network.nodes.find(n => n.id === this.topBridges[i].target);

      if (i < this.itemsInPanelCollapsed) {
        this.top3bridgesArray[i] = this.topBridgesString.concat((i + 1).toString() + '. ' + source.name + ' -> ' + target.name);
      } else {
        this.topBridgesString = this.topBridgesString.concat((i + 1).toString() + '. ' + source.name + ' -> ' + target.name);
        if (i !== this.topBridges.length - 1) {
          this.topBridgesString = this.topBridgesString.concat('\n');
        }
      }
    }

    console.log(`[graph-metrics] ${new Date().toISOString()} finished init... `);

  }

}
