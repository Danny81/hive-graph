import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphMetricsComponent } from './graph-metrics.component';

describe('GraphMetricsComponent', () => {
  let component: GraphMetricsComponent;
  let fixture: ComponentFixture<GraphMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
