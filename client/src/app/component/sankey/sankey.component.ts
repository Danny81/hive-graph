import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter, HostListener,
  Input,
  OnChanges, OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';

import * as d3 from 'd3';
import * as d3sankey from 'd3-sankey';
import { ILinkDetails } from '../../model/link-details';
import { INodeDetails } from '../../model/node-details';
import { INodeSankeyNodeDetails, NodeSankey } from '../../model/node-sankey';
import { DataService } from '../../service/data/data.service';

@Component({
  selector: 'app-sankey',
  templateUrl: './sankey.component.html',
  styleUrls: ['./sankey.component.scss'],
})
export class SankeyComponent implements AfterViewInit, OnChanges, OnDestroy {
  private static readonly MIN_NODE_HEIGHT = 2;

  @Input() public readonly egoNode: INodeDetails;
  @Input() public readonly sankey: NodeSankey;
  @Output() public readonly nodeClicked = new EventEmitter<INodeDetails>();

  private readonly _svgWidth = 1920;
  private readonly _svgHeight = 1080;

  private _svg: d3.Selection<SVGElement, void, HTMLElement, void>;
  private _svgContent: d3.Selection<SVGGElement, void, HTMLElement, void>;

  public constructor(private readonly _elementRef: ElementRef<HTMLElement>, private  readonly _data: DataService) {}

  public ngAfterViewInit(): void {
    this._svg = d3
      .select<HTMLElement, void>(this._elementRef.nativeElement)
      .select<SVGElement>('svg')
      .attr('viewBox', `0 0 ${this._svgWidth} ${this._svgHeight}`);
    this._svgContent = this._svg.append<SVGGElement>('g').classed('app-vis-content', true);
    this._svg.call(d3.zoom<any, void>().on('zoom', () => this._svgContent.attr('transform', d3.event.transform)));
    this.update();
    console.log(`[sankey] initialization finished, detection time ${this.egoNode.detectionTime} was received...`);

  }

  public ngOnChanges(changes: SimpleChanges): void {
    console.log('[sankey] changes', changes);
    if ('sankey' in changes) {
      this.update();
    }
  }

  private normalize(): void {
    let sourceSum = 0;
    let targetSum = 0;
    for (const link of this.sankey.links) {
      if (link.source === this.egoNode.id) {
        sourceSum += link.value;
      } else {
        targetSum += link.value;
      }
    }

    for (const link of this.sankey.links) {
      const sum = link.source === this.egoNode.id ? sourceSum : targetSum;
      link.originalValue = link.value;
      link.value = (link.value / sum) * 100;
    }

    console.log('[Sankey] normalized', this.sankey);
  }

  private update(): void {
    if (!this._svg) {
      return;
    }

    console.log('[Sankey] update', this.sankey);

    this.normalize();

    this._svgContent.html('');

    const self = this;
    const config = {
      margin: { top: 10, bottom: 10, left: 10, right: 10 },
      alignment: d3sankey.sankeyJustify,
      node: {
        drag: true,
        width: 15,
        padding: 10,
        stroke: '#111',
      },
      link: { color: '#AAA' },
    };

    const sankey = d3sankey
      .sankey<INodeDetails, ILinkDetails>()
      .nodeId(d => d.id)
      .nodeAlign(config.alignment)
      .nodeWidth(config.node.width)
      .nodePadding(config.node.padding)
      .extent([
        [config.margin.left, config.margin.top],
        [this._svgWidth - config.margin.right, this._svgHeight - config.margin.bottom],
      ]);

    const { nodes: nodesData, links: linksData } = sankey(this.sankey);

    let node: d3.Selection<any, INodeDetails, any, void>;
    const link = this._svgContent
      .append('g')
      .classed('app-links', true)
      .selectAll('g')
      .data(linksData)
      .join('g')
      .classed('app-link', true)
      .on('mouseover', function(d) {
        d3.select(this).classed('app-link-hovering', true);
        node.classed('app-link-hovering', nd => d.source === nd || d.target === nd);
      })
      .on('mouseout', function(d) {
        d3.select(this).classed('app-link-hovering', false);
        node.classed('app-link-hovering', false);
      });

    const linkPaths = link
      .append('path')
      .attr('d', d3sankey.sankeyLinkHorizontal())
      .attr('stroke', '#AAA')
      .attr('stroke-width', d => Math.max(SankeyComponent.MIN_NODE_HEIGHT, d.width));

    link
      .append('title')
      .text(d => `${(d.source as INodeSankeyNodeDetails).name} → ${(d.target as INodeSankeyNodeDetails).name}`);

    node = this._svgContent
      .append('g')
      .classed('app-nodes', true)
      .selectAll('g')
      .data(nodesData)
      .join('g')
      .classed('app-node', true)
      .attr('transform', d => `translate(${d.x0},${d.y0})`)
      .on('click', d => this.nodeClicked.emit(d))
      .on('mouseover', function(d) {
        d3.select(this).classed('app-node-hovering', true);
        link.classed('app-node-hovering', ld => ld.source === d || ld.target === d);
      })
      .on('mouseout', function(d) {
        d3.select(this).classed('app-node-hovering', false);
        link.classed('app-node-hovering', false);
      });

    if (config.node.drag) {
      node.classed('app-draggable', true).call(
        d3
          .drag<SVGGElement, INodeDetails>()
          .on('drag', function(d) {
            d.y0 += d3.event.dy;
            d.y1 += d3.event.dy;
            d3.select(this)
              .classed('app-node-dragging', true)
              .attr('transform', `translate(${d.x0},${d.y0})`);
            link.classed('app-node-dragging', ld => ld.source === d || ld.target === d);
            sankey.update(self.sankey);
            linkPaths.attr('d', d3sankey.sankeyLinkHorizontal());
          })
          .on('end', function() {
            d3.select(this).classed('app-node-dragging', false);
            link.classed('app-node-dragging', false);
          })
      );
    } else {
      node.classed('app-draggable', false);
    }

    node
      .append('rect')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', d => d.x1 - d.x0)
      .attr('height', d => Math.max(2, d.y1 - d.y0))
      .attr('stroke', config.node.stroke)
      .attr('fill', d => d.color);

    node.append('title').text(d => `${d.name}\n${d.subIndustry}`);
    node
      .append('text')
      .classed('app-label', true)
      .attr('x', d => (d.x0 < this._svgWidth / 2 ? config.node.width + 6 : -6))
      .attr('y', d => (d.y1 + d.y0) / 2 - d.y0)
      .attr('dy', '0.35em')
      .attr('text-anchor', d => (d.x0 < this._svgWidth / 2 ? 'start' : 'end'))
      .text(d => d.name);

    const completionTime = Date.now();
    const elapsedTime = completionTime - this.egoNode.detectionTime;
    console.log(`[main] time now ${ Date.now()} ms`);
    const sepp = this._data.addMeasurement('DetailsOnDemand', this.egoNode.id, this.egoNode.detectionTime, completionTime, elapsedTime).pipe().subscribe(
      result => {
        console.log(result);
      },
      e => {
        console.error('[main] could not add measurement', e);
      },
    );
  }

  @HostListener('unloaded')
  public ngOnDestroy(): void{
    console.log('[sankey] destroyed');
  }


}
