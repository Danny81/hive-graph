import { AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ClickedNode, ClickedNodes } from '../../model/clicked-nodes';
import { NetworkNode } from '../../model/network';
import { INodeDetails } from '../../model/node-details';

@Component({
  selector: 'app-details-on-demand',
  templateUrl: './details-on-demand.component.html',
  styleUrls: ['./details-on-demand.component.scss'],
})
export class DetailsOnDemandComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() public readonly reloading = false;
  @Input() public readonly node: INodeDetails;
  @Input() public readonly currentEgoNode: NetworkNode;
  @Input() public clickedNodes: ClickedNodes;
  @Output() public readonly hideDetailsOnDemand = new EventEmitter<void>();

  public ngOnInit(): void {

  }

  public ngOnChanges(changes: SimpleChanges): void {
    console.log('[details on demand] changes', changes);
  }

  public close(event: MouseEvent) {
    console.log('[details on demand] closing details on demand...');
    event.stopPropagation();
    this.hideDetailsOnDemand.emit();
  }

  public ngAfterViewInit() {
    console.log(`[details on demand] init finished`);
  }

}
