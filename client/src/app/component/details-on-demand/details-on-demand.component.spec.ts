import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOnDemandComponent } from './details-on-demand.component';

describe('DetailsOnDemandComponent', () => {
  let component: DetailsOnDemandComponent;
  let fixture: ComponentFixture<DetailsOnDemandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOnDemandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOnDemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
