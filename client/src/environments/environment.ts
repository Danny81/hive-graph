export const environment = {
  production: false,
  server: {
    host: 'http://10.21.9.60',
    port: 8080,
  },
  visConfig: {
    defaultSubIndCode: '#NV',
    ego: {
      id: 'voe av',
      minRadius: 40,
      egoDepth: '1',
      initCompetitorToken: 2,
    },
    competition: {
      minRadius: 23,
    },
    node: {
      minRadius: 15,
      scaleFactor: 6,
    },
  },
  graphMetrics: {
    totalItemsInExpansionPanel: 40,
    numItemsVisibleCollapsed: 3,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
