export const environment = {
  production: true,
  server: {
    host: 'http://ec2-18-220-127-211.us-east-2.compute.amazonaws.com',
    port: 8080,
  },
  visConfig: {
    defaultSubIndCode: '#NV',
    ego: {
      id: 'voe av',
      minRadius: 40,
      egoDepth: '1',
      initCompetitorToken: 2,
    },
    competition: {
      minRadius: 23,
    },
    node: {
      minRadius: 15,
      scaleFactor: 6,
    },
  },
  graphMetrics: {
    totalItemsInExpansionPanel: 40,
    numItemsVisibleCollapsed: 3,
  },
};
