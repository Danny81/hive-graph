# JRC-Live - Voest: Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.

Production routes:

`http://jrclivegraphdemo01.s3-website.us-east-2.amazonaws.com/` - graph visualization

`http://jrclivegraphdemo01.s3-website.us-east-2.amazonaws.com/#/admin` - dataset import facility

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Deployment to AWS

1. Log into the AWS CLI with a user that has sufficient privileges

2. Run `ng build --prod`

3. Run `npm run aws-empty` or  `aws s3 rm s3://jrclivegraphdemo01 --recursive` to delete the old sources from the S3 bucket

4. Run `npm run aws-deploy` or `aws s3 sync dist/jrclive-voest s3://jrclivegraphdemo01` to deploy the new sources on Amazon S3

New version should now run under `http://jrclivegraphdemo01.s3-website.us-east-2.amazonaws.com/`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
